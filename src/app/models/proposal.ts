export interface ProposalListingApiResponce {
  resCode: number;
  success: boolean;
  totalCount: number;
  totalPage: number;
  data: Array<ProposalINFO>;
}
export interface ProposalINFO {
  id: number;
  current_page: number;
  from: number;
  per_page: number;
  to: number;
  total: number;
  first_page_url: string;
  last_page_url: string;
  next_page_url: string;
  prev_page_url: string;
  path: string;
  data: Array<Proposals>;
}
export interface Proposals {
  id: number;
  user_id: number;
  proposal_unique_id: number;
}
export interface FacilityINFO {
  permission: Array<PermissionINFO>;
  id: number;
  name: string;
  status: boolean;
  url: string;
}
export interface PermissionINFO {
  id: number;
  permissionName: string;
  status: boolean;
}

export interface EDITProposalApiResponce {
  resCode: number;
  success: boolean;
  message: string;
  result: string;
}
export interface ADDProposalApiResponce {
  resCode: number;
  success: boolean;
  message: string;
  result: string;
}
