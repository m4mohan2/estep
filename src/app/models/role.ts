export interface RoleListingApiResponce {
  resCode: number;
  data: Array<RoleINFO>;
}
export interface RoleINFO {
  id: number;
  roleName: string;
  facility: Array<FacilityINFO>;
}
export interface FacilityINFO {
  permission: Array<PermissionINFO>;
  id: number;
  name: string;
  status: boolean;
  url: string;
}
export interface PermissionINFO {
  id: number;
  permissionName: string;
  status: boolean;
}

export interface EDITRoleApiResponce {
  resCode: number;
  success: boolean;
  message: string;
  result: string;
}
export interface ADDRoleApiResponce {
  resCode: number;
  success: boolean;
  message: string;
  result: string;
}
