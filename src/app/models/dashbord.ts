export interface DashbordApiResponce {
  success: boolean;
  result: string;
  message: string;
  resCode: number;
  data: DashbordInfo;
}
export interface DashbordInfo {
  image: string;
  image_hover: string;
  title: string;
  count: number;
  bargraphtag: string;
}
export interface LgaWiseReportApiResponce {
  success: boolean;
  resCode: number;
  page: number;
  totalPage: number;
  totalCount: number;
  data: Array<LgaWiseReportInFo>;
}
export interface LgaWiseReportInFo {
  lga: string;
  Screened: string;
  Presumptive: string;
  Treatment: string;
  Positive: string;
  Negative: string;
  active: string;
  lapsed: string;
}
export interface StateWiseReportApiResponce {
  success: boolean;
  resCode: number;
  name: Array<any>;
  data: Array<any>;
  dataAsNumber: Array<any>;
}
export interface AdminDashboardGraphApiResponce {
  success: boolean;
  result: string;
  message: string;
  resCode: number;
  title: string;
  bargraphtag: string;
  data: AdminDashboardGraphINFO;
}
export interface AdminDashboardGraphINFO {
  date: Array<any>;
  data: Array<any>;
}
