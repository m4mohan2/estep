import { Directive, ElementRef, OnInit, HostListener, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appRandomBackground]'
})
export class RandomBackgroundDirective implements OnInit {
  backgroudcolor: Array<any> = ['#4CAF50', '#EF5350', '#009688', '#FFA726', '#FFA726'];
  catagoriesbackground: Array<any>;
  colorArray: Array<any>;
  @Input() colorType: string;
  constructor(private el: ElementRef) {
    this.catagoriesbackground = ['rgbA(76, 175, 80, .85)',
      'rgbA(239, 83, 80, .85)',
      'rgbA(0, 150, 136, .85)',
      'rgbA(255, 167, 38, .85)',
      'rgbA(41, 182, 246, .85)'
    ];

  }
  ngOnInit() {
    this.changecolor(this.colorType);
  }
  // /**
  //    *
  //    * @returns  random color from the array backgroudcolor
  //    * @memberof RandomBackground
  //    */
  changecolor(colorType) {
    if (colorType === 'rgba') {
      this.colorArray = this.catagoriesbackground;
    } else {
      this.colorArray = this.backgroudcolor;
    }
    this.el.nativeElement.style.backgroundColor = this.colorArray[Math.floor(Math.random() * this.colorArray.length)];
  }

}
