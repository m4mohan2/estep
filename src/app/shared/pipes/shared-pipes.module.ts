import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateSuffixPipe } from './date-suffix.pipe';
import { ExcerptPipe } from './excerpt.pipe';
import { OrdinalPipe } from './ordinal.pipe';
import { UsaPhonePipe } from './usa-phone.pipe';
import { ToDateObjPipe } from './to-date-obj.pipe';

const pipes = [
  DateSuffixPipe,
  ExcerptPipe,
  OrdinalPipe,
  UsaPhonePipe,
  ToDateObjPipe
];

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: pipes,
  exports: pipes
})
export class SharedPipesModule { }