import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastComponent } from './toast.component';
import { ToastProvider } from './toast.provider';
import { MatSnackBarModule, MatIconModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatSnackBarModule,
    MatIconModule
  ],
  declarations: [
    ToastComponent
  ],
  exports: [
    CommonModule,
  ],
  providers: [ToastProvider],
  entryComponents: [ToastComponent]
})
export class ToastModule { }
