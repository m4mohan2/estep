import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';
import {tap} from 'rxjs/operators';

import { DashBordService } from 'src/app/services/apis/dashbord.service';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable()
export class VideoResolver implements Resolve<any> {
	
  video_list:any = [];
  constructor(
  	private dasboardApi:DashBordService,
  	public _sanitizer: DomSanitizer
  	) {}

  resolve(route: ActivatedRouteSnapshot) {

  	this.dasboardApi.getVideos().subscribe( (res: any) => {

        this.video_list.splice(0, this.video_list.length);

        for (let i = 0; i < res.length; i++) {
            this.video_list.push(this._sanitizer.bypassSecurityTrustResourceUrl(res[i].videoCode));
         } 

        },error=>{}
    );
    return this.video_list;
  }
}