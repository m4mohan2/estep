import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { MatDialog } from '@angular/material';
import { ChangepasswordComponent } from '../../modules/post-auth/change-password/change-password.component';
import { AuthService } from 'src/app/services/apis/auth.service';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { LogOutApiResponce } from 'src/app/models/auth';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from 'src/app/services/apis/common.service';
import { NgxUiLoaderService, SPINNER } from 'ngx-ui-loader';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [AuthService, ToastProvider]
})
export class HeaderComponent implements OnInit {
  spinnerType = SPINNER.rectangleBounce;
  @Output() navToggle = new EventEmitter<boolean>();
  userInfo: any;
  param: any;
  language;
  constructor(
    private router: Router,
    private userStorage: UserStorageProvider,
    private dialog: MatDialog,
    private authApi: AuthService,
    private toast: ToastProvider,
    private translateService: TranslateService,
    private common:CommonService,
    private ngxService:NgxUiLoaderService
  ) {
    this.userInfo = this.userStorage.get();
  }

  ngOnInit() {
    this.common.currLang.subscribe(
      res=>{
        this.language = res
      }
    )
    const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    if (width > 1024) {
      this.navToggle.emit(true);
    }
  }

  navBtnClicked() {
    this.navToggle.emit(true);
  }
  logOut() {
    //this.authApi.logout(this.param).subscribe((responseData: LogOutApiResponce) => {
      //if (responseData.success) {
        this.userStorage.clear();
        this.router.navigate(['/']);
        //this.toast.success(responseData.message);
        
      //}
    /* }, error => {
    }); */
  }
  /*
  * function use to open chnage password modal
  *
  * @memberof PostAuthComponent
  */
  changePassword() {
    const dialogRef = this.dialog.open(ChangepasswordComponent, {
      panelClass: 'dialog-xs',
      width: '600px',
      disableClose: true,
      // data: this.catagoriesListing[catagoryIndex]
    });
  }
  editProfile(){
    this.router.navigate(['/post-auth/edit-profile/']);
  }

  changeLang(language: string) {
		this.language = language;
		this.common.currLang.next(this.language);
    this.translateService.use(this.language);
  }

}
