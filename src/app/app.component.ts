import { Component } from '@angular/core';
import { Router} from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { UserStorageProvider } from './services/storage/user-storage.service';
import { UserMasterService } from './services/apis/usermaster';
import { AppConst } from 'src/app/app.constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'cecfl';
  searchParam: any;
  constructor(
    public router: Router,
    public translateService: TranslateService,
    private userStorage: UserStorageProvider,
    private UserMasterApi: UserMasterService,
    ) {
      if(AppConst.APP_MODE == 'Production') {
        translateService.setDefaultLang('es');
      } else {
        translateService.setDefaultLang('en');
      }
    }

    ngOnInit() {
      this.getCityList();
    }

    getCityList() {
      this.searchParam = {
        search: 'PR',
      };
      this.UserMasterApi.citySearch(this.searchParam).subscribe((res: any) => {
        if (res.success) {
          this.userStorage.setCity(res.data);
        }
      }, (err) => {
      });
      
    }
  
  }

  


