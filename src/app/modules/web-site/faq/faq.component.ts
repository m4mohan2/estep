import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DashBordService } from 'src/app/services/apis/dashbord.service';
import { CommonService } from 'src/app/services/apis/common.service';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
    isShown1: boolean = true ;
    isShown2: boolean = false ;
    isShown3: boolean = false ;
    commissionData:any = []
    commissionParam:any = {};
    language
    constructor(
        private myRoute: Router,
        private dasboardApi:DashBordService,
        private common:CommonService
    ) { }

    ngOnInit() {
        this.common.currLang.subscribe(
            res=>{
                this.language = res
            }
        )
        this.getCmsContent()
    }
    faq1Show(){
        this.isShown1 = true
        this.isShown2 = false
        this.isShown3 = false
    }
    faq2Show(){
        this.isShown1 = false
        this.isShown2 = true
        this.isShown3 = false
    }
    faq3Show(){
        this.isShown1 = false
        this.isShown2 = false
        this.isShown3 = true
    }
    navigate=(param)=>{
        this.myRoute.navigate([param]);
    }
    getCmsContent(){
        this.commissionParam.short_code='frequently-asked-questions'
        this.dasboardApi.getCmsContent(this.commissionParam).subscribe(
            res=>{
                this.commissionData = res
            },
            error=>{

            }
        )
    }

}
