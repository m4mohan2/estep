import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DashBordService } from 'src/app/services/apis/dashbord.service';
import { CommonService } from 'src/app/services/apis/common.service';

@Component({
  selector: 'app-providers',
  templateUrl: './providers.component.html',
  styleUrls: ['./providers.component.scss']
})
export class ProvidersComponent implements OnInit {
    commissionData:any = []
    commissionParam:any = {};
    language
    constructor(
        private myRoute: Router,
        private dasboardApi:DashBordService,
        private common:CommonService
    ) { }

    ngOnInit() {
        this.common.currLang.subscribe(
            res=>{
                this.language = res
            }
        )
        this.getCmsContent()
    }
    navigate=(param)=>{
        this.myRoute.navigate([param]);
    }
    getCmsContent(){
        this.commissionParam.short_code='services-and-service-providers'
        this.dasboardApi.getCmsContent(this.commissionParam).subscribe(
            res=>{
                this.commissionData = res
            },
            error=>{

            }
        )
    }

}
