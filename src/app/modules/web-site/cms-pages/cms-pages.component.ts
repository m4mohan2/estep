import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute,  Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { CommonService } from 'src/app/services/apis/common.service';
import { DashBordService } from 'src/app/services/apis/dashbord.service';

@Component({
  selector: 'app-cms-pages',
  templateUrl: './cms-pages.component.html',
  styleUrls: ['./cms-pages.component.scss']
})
export class CmsPagesComponent implements OnInit {
  faqData:any = [];
  faqParam:any = {};
  pages: string;
  language;
  constructor(
    private route: ActivatedRoute,
    private myRoute: Router,
    private common:CommonService,
    private dasboardApi:DashBordService,
  ) { 
    myRoute.events.subscribe( (event: Event) => {
        if (event instanceof NavigationEnd) {
           this.getCmsContent();
        }
    });
  }

  ngOnInit() {
      this.common.currLang.subscribe(
        res=>{
            this.language = res
        }
    )
    //this.getCmsContent()
  }

  getCmsContent(){
    this.route.params.subscribe(params => {
      this.pages = params['pages'];
      
    })
    this.faqParam.short_code=this.pages;
    //console.log(this.faqParam);
    
    this.dasboardApi.getCmsContent(this.faqParam).subscribe(
        res=>{
            this.faqData = res
        },
        error=>{

        }
    )
  }

}
