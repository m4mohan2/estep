import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/apis/common.service';
import { TranslateService } from '@ngx-translate/core';
import { ProjectService } from 'src/app/services/apis/project.service';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { AppConst } from 'src/app/app.constants';
import { interval } from 'rxjs';
import { map } from 'rxjs/operators'
import { UserMasterService } from 'src/app/services/apis/usermaster';



@Component({
  selector: 'app-home-header',
  templateUrl: './home-header.component.html',
  styleUrls: ['./home-header.component.scss']
})
export class HomeHeaderComponent implements OnInit {

  showSearch: boolean = false;
  showTopNav: boolean = true;
  language;
  projectParam : any={}
  projectData : any=[]
  userInfo: any;
  projectUrl: any;
  remainDays : any;
  remainHrs : any;
  remainMin : any;
  remainSec : any;
  barData: Array < any > ;
  constructor( 
    private myRoute: Router,
    private common: CommonService,
    private translateService: TranslateService,
    private projectApi: ProjectService,
    private userStorage: UserStorageProvider,
    private router: Router,
    private UserMasterApi: UserMasterService,
  ) { 
    this.userInfo = this.userStorage.get();
    this.projectUrl = AppConst.PROJECT_FOLDER;
  }

  ngOnInit() {

    this.getCategoryList();

    this.common.currLang.subscribe(
      res =>{
        this.language = res
      }
    )

    this.latestProject()

    // window.onresize = function() {
    //   if (window.innerWidth  < 767) { 
    //     document.getElementById('menu').style.display = 'none';
    //   }
    // }
    

  }
  

  //mobile menu toggle
  menuToggle() {
    var x = document.getElementById("menu");
    if (x.style.display === "block") {
      x.style.display = "none";
    } else {
      x.style.display = "block";
    }
  } 


  // Top Search Show Hide
  toggleSearch() {
    this.showSearch = !this.showSearch;
    this.showTopNav = !this.showTopNav;
  }




  navigateRegister(){
    this.myRoute.navigateByUrl('/pre-auth/registered')
  }
  navigateLogin(){
    this.myRoute.navigateByUrl('/pre-auth/login')
  }
  elegibletoapplyLink(){
    this.myRoute.navigateByUrl('/pages/who-eligible-to-apply')
  }

  navigateHome(){
    this.myRoute.navigateByUrl('/home')
  }

  navigate=(param)=>{
      this.myRoute.navigate([param]);
  }


  changeLang(language: string) {
    console.log("language",language)
		this.language = language;
		this.common.currLang.next(this.language);
    this.translateService.use(this.language);
  }

  latestProject(){
    this.projectParam.page = 1;
    this.projectApi.activeProject().subscribe(
      res=>{
        if (res.data.length > 0) {
          
          this.projectData = res.data;        
          let self = this;


          setInterval(function(){ 

            let endDateNew ;
            let endDate ;
            let curDate ;

            //let endDate1 ;
            //let curDate1 ;  

            curDate = new Date();  
            //endDate = new Date(self.projectData[0].endDate);

            endDate = self.projectData[0].endDate;

            if (endDate.toString().indexOf(' ') === -1) {
                endDate = endDate + ' 00:00:00';
            }
            const temp = endDate.toString().replace(' ', 'T');
            
            endDateNew = new Date(temp);

            //console.log("curDate=>", curDate);
            //console.log("endDate=>", endDateNew);

            //console.log("curDate US=>", curDate.toLocaleString('en-US', {timeZone: 'America/Los_Angeles'}));
            //console.log("curDate US=>", curDate.toLocaleString('en-US', {timeZone: 'America/Los_Angeles'}));

            //console.log("endDate us=>", endDate.toLocaleString('en-US', {timeZone: 'America/Los_Angeles'}));
            
            //endDate1 = endDate.toLocaleString('en-US', {timeZone: 'America/Los_Angeles'});
            //curDate1 = curDate.toLocaleString('en-US', {timeZone: 'America/Los_Angeles'});

            //console.log("curgetTime=>", curDate.getTime());





            let res = Math.abs(endDateNew - curDate) / 1000;
            //console.log('res=>',res)    


            //let res1 = Math.abs(endDate1.getTime() - curDate1.getTime()) / 1000;
            //console.log('res1=>',res1)            



            // get total days between two dates
            self.remainDays = Math.floor(res / 86400);                                 
            // get hours        
            self.remainHrs = Math.floor(res / 3600) % 24;               
            // get minutes
            self.remainMin = Math.floor(res / 60) % 60;          
            // get seconds
            self.remainSec = Math.floor(res % 60); 
            
          }, 1000);
        }
      }
    )
  }

  toDateObj = function(value) {
    if (value) {
        if (value.toString().indexOf(' ') === -1) {
            value = value + ' 00:00:00';
        }
        const temp = value.toString().replace(' ', 'T');
        return new Date(temp);
    } else {
        return null;
    }
  }

  getCategoryList() {
    this.UserMasterApi.getCategoryReport(false).subscribe((res: any) => {
      if (res.success) {
        this.barData = res.data;
              }
    }, (err) => {});
  }



}
