import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OurCommissionComponent } from './our-commission.component';

describe('OurCommissionComponent', () => {
  let component: OurCommissionComponent;
  let fixture: ComponentFixture<OurCommissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OurCommissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OurCommissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
