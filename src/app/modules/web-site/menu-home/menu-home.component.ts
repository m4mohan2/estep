import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu-home',
  templateUrl: './menu-home.component.html',
  styleUrls: ['./menu-home.component.scss']
})
export class MenuHomeComponent implements OnInit {

  constructor( private myRoute: Router  ) { }

  ngOnInit() {
  }

  navigateHome(v){
    this.myRoute.navigateByUrl('/home' + '#' + v)
  }

}
