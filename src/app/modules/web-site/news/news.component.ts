import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NewsService } from 'src/app/services/apis/news.service';
import { HttpModule } from '@angular/http';
import { AppConst } from 'src/app/app.constants';
import { CommonService } from 'src/app/services/apis/common.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  newsParam : any={}
  newsData : any=[]
  language;
  newsOptions;

  constructor(
    private newsApi: NewsService,
    private common: CommonService,
    private route: Router
  ) { }

  ngOnInit() {
    this.common.currLang.subscribe(
      res=>{
        this.language = res
      }
    )
    this.latestNews();
    this.newsOptions = { items: 1, autoplay: true, dots: true, nav:true, navigation: true, loop: true, animateOut: 'fadeOut', animateIn: 'fadeIn', autoplayHoverPause: true };
  }

  getImgUrl(imagename) {
    // console.log(imagename);
    if (imagename && imagename !== '') {
      return AppConst.API_BASE_URL + '/' + imagename;
    } else {
      return 'assets/images/no-img.png';
    }

  }

  latestNews(){
    this.newsParam.page = 1;
    this.newsApi.activenews(this.newsParam).subscribe(
      res=>{
        //console.log(res);
        this.newsData=res.data
      }
    )
  }

  newsDetailsLink(slug){
    this.route.navigateByUrl('/news/'+slug)
  }

}
