import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/services/guards/auth-guard.service';
import { HomeComponent } from './home.component';
const routerConfig: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
];
@NgModule({
  imports: [
    RouterModule.forChild(routerConfig)
  ],
  exports: [RouterModule],
  declarations: [],
  providers: [AuthGuard]
})
export class HomeRouteModule { }
