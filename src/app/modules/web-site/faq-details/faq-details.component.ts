import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/apis/common.service';
import { DashBordService } from 'src/app/services/apis/dashbord.service';

@Component({
  selector: 'app-faq-details',
  templateUrl: './faq-details.component.html',
  styleUrls: ['./faq-details.component.scss']
})
export class FaqDetailsComponent implements OnInit {
  faqData:any = []
  faqParam:any = {};
  language
  constructor(
    private myRoute: Router,
    private common:CommonService,
    private dasboardApi:DashBordService,
  ) { }

  ngOnInit() {
      this.common.currLang.subscribe(
        res=>{
            this.language = res
        }
    )
    this.getCmsContent()
  }

  getCmsContent(){
    this.faqParam.short_code='faq-details'
    this.dasboardApi.getCmsContent(this.faqParam).subscribe(
        res=>{
            this.faqData = res
        },
        error=>{

        }
    )
  }

}
