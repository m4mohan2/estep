import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute,  Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { CommonService } from 'src/app/services/apis/common.service';
import { NewsService } from 'src/app/services/apis/news.service';
import { AppConst } from 'src/app/app.constants';
import { NgxUiLoaderService, SPINNER } from 'ngx-ui-loader';

import { MatDialog, MatTableDataSource, MatPaginator, PageEvent, MatSort } from '@angular/material';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss']
})
export class NewslistComponent implements OnInit {
  language;

  totalItem: number;
  page: number = 1;
  itemsPerPage:number = 10;
  searchParam: any = {};

  searchList :  any = {
      data: []
  }

  spinnerType = SPINNER.rectangleBounce;
  constructor(
    private route: ActivatedRoute,
    private myRoute: Router,
    private common:CommonService,
    private newsApi:NewsService,
    private ngxService :NgxUiLoaderService,
  ) { 
    this.searchList =[];
  }

  ngOnInit() {
      this.common.currLang.subscribe(
        res=>{
            this.language = res
        }
    )
    this.searchParam.page = this.page;
    this.searchParam.itemsPerPage = this.itemsPerPage;
    this.fetchListdata();
  }
 

  fetchListdata() {
    this.ngxService.start();
    this.newsApi.activenews(this.searchParam).subscribe((res) => {
      if (res) { 
        this.totalItem = res['total'];       
        this.searchList = res.data;
        this.ngxService.stop();
      }
    }, error => {
      this.ngxService.stop();
      this.searchList = [];
    });
  }

  getNext(event: PageEvent) {
      //console.log('getNext event', event);
      this.page = event.pageIndex + 1;
      this.searchParam.page = event.pageIndex + 1;
      this.fetchListdata();
  }

  newsDetailsLink(slug){
    this.myRoute.navigateByUrl('/news/'+slug)
  }

  getImgUrl(imagename) {
    // console.log(imagename);
    if (imagename && imagename !== '') {
      return AppConst.API_BASE_URL + '/' + imagename;
    } else {
      return 'assets/images/no-img.png';
    }

  }

}
