import { NgModule } from '@angular/core';

import { ComponentsModule } from 'src/app/components/components.module';
import { AppMaterialModule } from '../../app.material.module';
import { AuthService } from '../../services/apis/auth.service';
import { SharedModule } from '../../shared/shared.module';
import { NgxMaskModule } from 'ngx-mask';
import { FormsModule } from '@angular/forms';
import { OwlModule } from 'ngx-owl-carousel';
import { ChartsModule, ThemeService } from 'ng2-charts';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { WebsiteRouteModule } from './web-site.routes.module';
import { webSiteComponent } from './web-site.component';

import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { NewslistComponent } from './news-list/news-list.component';
import { SearchMapViewComponent } from './search-map-view/search-map-view.component';
import { SearchDetailsComponent } from './search-details/search-details.component';
import { OurCommissionComponent } from './our-commission/our-commission.component';
import { GeneralPublicComponent } from './general-public/general-public.component';
import { OrganizationChartComponent } from './organization-chart/organization-chart.component';
import { InnearHeaderComponent } from '../../components/innear-header/innear-header.component';
import { NewsService } from 'src/app/services/apis/news.service';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppConst } from 'src/app/app.constants';
import { CommonService } from 'src/app/services/apis/common.service';
import { ProjectService } from 'src/app/services/apis/project.service';
import { DashBordService } from 'src/app/services/apis/dashbord.service';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgxPaginationModule } from 'ngx-pagination';


export function HttpLoaderFactory(HttpClient: HttpClient) {
  return new TranslateHttpLoader(HttpClient,AppConst.PROJECT_FOLDER+'assets/i18n/', '.json');
}

@NgModule({
  imports: [
    WebsiteRouteModule,
    ComponentsModule,
    AppMaterialModule,
    SharedModule,
    NgxUiLoaderModule,
    NgxMaskModule.forRoot(),
    FormsModule,
    OwlModule,
    ChartsModule,
    InfiniteScrollModule,
    NgxPaginationModule, 
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ],
  declarations: [
    HomeComponent,
    webSiteComponent, 
    SearchComponent,
    SearchMapViewComponent,
    OurCommissionComponent,
    GeneralPublicComponent,
    OrganizationChartComponent,
    NewslistComponent
  ],
  providers: [AuthService, NewsService, PermissionProvider, CommonService, ProjectService, DashBordService, ThemeService],
  entryComponents: [
    
  ],

})
export class WebSiteModule { }
