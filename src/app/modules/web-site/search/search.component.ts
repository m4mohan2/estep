import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { NgxUiLoaderService, SPINNER } from 'ngx-ui-loader';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from 'src/app/services/apis/common.service';

declare var google: any;

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  barChart:boolean = false;
  public StaticColor: any;
  public barchartObj: any;
  Citylisting: Array < any > ;
  barData: Array < any > ;
  totOrg: any;
  searchParam: any;
  searchCity: any;
  language;
  cityId : string;
  category : string;
  keyword : string;
  showNoData : boolean = false;
  spinnerType = SPINNER.rectangleBounce;
  locations : Array<any>;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private UserMasterApi: UserMasterService,
    private ngxService :NgxUiLoaderService,
    private translate: TranslateService,
    private common: CommonService,
    ) { 
    this.barchartObj = {};
    this.Citylisting = [];
    this.getCityList();
    this.getCategoryReportChart();
    this.StaticColor = ['#5271ba', '#3da3e8', '#51c0bf', '#fbb03b', '#f47b50',
    '#5271ba', '#3da3e8', '#51c0bf', '#fbb03b', '#f47b50',
    '#5271ba', '#3da3e8', '#51c0bf', '#fbb03b', '#f47b50',
    '#5271ba', '#3da3e8', '#51c0bf', '#fbb03b', '#f47b50',
    '#5271ba', '#3da3e8', '#51c0bf', '#fbb03b', '#f47b50',
    '#5271ba', '#3da3e8', '#51c0bf', '#fbb03b', '#f47b50',
    ];
  }
  ngOnInit() {
    
    this.cityId = '';
    this.category = '';
    this.keyword = '';
    
    this.fetchListdata();
    this.locations = [];

    this.common.currLang.subscribe(
      res =>{
        this.language = res
        this.getCategoryReportChart();
      }
    )
  }

  getCityList() {
    this.searchParam = {
      search: 'PR',
    };
    this.UserMasterApi.citySearch(this.searchParam).subscribe((res: any) => {
      if (res.success) {
        this.Citylisting = res.data;
      }
    }, (err) => {});
    this.gotoTop();
  }

  cityChangeEvent(v) {
    this.searchCity = {
      search: v,
    };
    this.getCategoryReportChart();
  }

  getCategoryReportChart() {
    this.UserMasterApi.getCategoryReport(this.searchCity).subscribe((res: any) => {
      if (res.success) {
        this.barData = res.data;
        this.totOrg = res.totalOrg;
        var labelList = [];
        var dataList = [];

        for (let item of res.data) {
          labelList.push(this.translate.instant('REGISTRATION.CATEGORY_OPT.' + item.category));
          //labelList.push(item.category);
          dataList.push(item.tot);
        }
        this.setBarChart(labelList, dataList);
      }
    }, (err) => {});
  }

  setBarChart(label?, datas?) {
      this.barChart =  true;
      this.barchartObj.barChartOptions = {
          scaleShowVerticalLines: false,
          responsive: true,
          legend: false,
          scales: {
              xAxes: [{
                  barPercentage: 1,
                  barThickness: 25,
                  maxBarThickness: 35,
                  minBarLength: 2,
                  gridLines: {
                    offsetGridLines: false,
                    display: false
                  }
              }],
              yAxes: [{
                  ticks: {
                    beginAtZero: true
                  }
              }]
          }
      };
      this.barchartObj.barChartLabels = label ? label : [];
      this.barchartObj.barChartType = 'bar';
      this.barchartObj.barChartLegend = true;
      this.barchartObj.barChartPlugins = [];
      this.barchartObj.barChartData = [
        {
          data: datas ? datas : [],
          backgroundColor: this.StaticColor,
          hoverBackgroundColor: this.StaticColor
        }
      ];
  }

  gotoTop() {
    window.scrollTo(0, 0);
  }


  fetchListdata() {
    this.ngxService.start();
    this.UserMasterApi.getSearchList(this.cityId, this.category, this.keyword).subscribe((res) => {
      if (res.success) {        
        if(res.data.length > 0){
          for(let i=0;i<res.data.length;i++){
            this.locations = res.data
            // const latLng = {lat : res.data[i].latitude,lng : res.data[i].longitude};
            // this.locations.push(latLng);            
          } 
          this.showNoData = false;
          setTimeout(() => {
            this.loadMap();  
          }, 200);                
          
        }else{
          this.locations = [];
          this.showNoData = true;
        }
        setTimeout(() => {
          this.ngxService.stop();
        }, 1000);        
      }
    }, error => {
      this.ngxService.stop();
      this.locations = [];
      this.showNoData = true;
    });
  }
  loadMap() {
    let map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10.1,
      center: new google.maps.LatLng(18.258249, -66.517703),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    let marker, i;
    for (i = 0; i < this.locations.length; i++) { 
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(this.locations[i].latitude, this.locations[i].longitude),
        map: map
      });
      var self = this;
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          self.router.navigateByUrl('/search-details', { state: self.locations[i] });
        }
      })(marker, i));
    }
    
  }

}
