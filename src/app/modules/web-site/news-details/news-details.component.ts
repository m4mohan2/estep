import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute,  Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { CommonService } from 'src/app/services/apis/common.service';
import { NewsService } from 'src/app/services/apis/news.service';
import { AppConst } from 'src/app/app.constants';



@Component({
  selector: 'app-news-details',
  templateUrl: './news-details.component.html',
  styleUrls: ['./news-details.component.scss']
})
export class NewsDetailsComponent implements OnInit {
  newsData:any = []
  newsParam:any = {};
  newsTitle: string;
  language;
  constructor(
    private route: ActivatedRoute,
    private myRoute: Router,
    private common:CommonService,
    private newsApi:NewsService,
  ) { 
    myRoute.events.subscribe( (event: Event) => {
      if (event instanceof NavigationEnd) {
         this.getNewsContent();
      }
    });

  }

  ngOnInit() {
    this.common.currLang.subscribe(
      res=>{
          this.language = res
      }
  )
  this.getNewsContent();
  
}

getNewsContent(){
    this.route.params.subscribe(params => {
      this.newsTitle = params['slug'];
      
    })

    this.newsParam.short_code=this.newsTitle
    this.newsApi.viewBySlug(this.newsTitle).subscribe(
        res=>{
            this.newsData = res
        },
        error=>{

        }
    )
  }

  getImgUrl(imagename) {
    // console.log(imagename);
    if (imagename && imagename !== '') {
      return AppConst.API_BASE_URL + '/' + imagename;
    } else {
      return 'assets/images/no-img.png';
    }

  }

}
