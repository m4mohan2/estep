import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  pageurl:any;
  constructor( private myRoute: Router ) { }

  ngOnInit() {
    const urlArr = this.myRoute.url.split('/');

    this.pageurl= urlArr[1];
    
    //console.log(urlArr[1])
    
  }

  navigate=(param)=>{
        this.myRoute.navigate([param]);
    }

  navigateHome(v=''){
    this.myRoute.navigateByUrl('/home' + '#' + v)
    //this.myRoute.navigateByUrl('/home')
  }

}
