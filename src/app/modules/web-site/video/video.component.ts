import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/services/apis/common.service';
import { DashBordService } from 'src/app/services/apis/dashbord.service';
import { DomSanitizer } from '@angular/platform-browser';

//import * as $ from 'jquery';
declare var $: any;

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})

//@ViewChild('owlElementVideo', {static: false} ) owl: ElementRef
export class VideoComponent implements OnInit {
  video_list:any = [];
  dummyArray:any= [];
  videoOptions;
	language;
	safeURL_en;
	safeURL_es;

  constructor(
    private route: ActivatedRoute,
  	private myRoute: Router,
    private common:CommonService,
    private dasboardApi:DashBordService,
    public _sanitizer: DomSanitizer
  	) { }

  ngOnInit() {
    this.common.currLang.subscribe(
        res=>{
            this.language = res
        }
    )
    this.getVideos();
    
    //this.video_list = this.route.snapshot.data.videoArr;

    //console.log("dynamic video_list=",this.video_list);


    /*this.dummyArray = [
        {name:'https://www.youtube.com/embed/hFpXv4eVfjo'},
        {name:'https://www.youtube.com/embed/EB1_EmIql38'},
        {name:'https://www.youtube.com/embed/NjAqE77JhJQ'},
     ];

    for (let i = 0; i < this.dummyArray.length; i++) {
      this.video_list.push(this._sanitizer.bypassSecurityTrustResourceUrl(this.dummyArray[i].name));      
    }*/

    //console.log("static video_list=",this.video_list);

    this.videoOptions={items: 1, margin:10, autoplay: false, dots: true, nav: true , navigation: true, loop: true, autoplayHoverPause: true,animateOut: 'fadeOut', animateIn: 'fadeIn', video: true, lazyLoad:true,
      navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
    };

  }

  /*ngAfterViewInit() {
      setTimeout(function () {
        $('.owl-carousel').owlCarousel({
            autoplay: true,
        });
      }, 2000);
  }*/

  getVideos(){
    this.dasboardApi.getVideos().subscribe( (res: any) => {
        for (let i = 0; i < res.length; i++) {
            //console.log("getVideos videoCode=>" , res[i].videoCode);
            this.video_list.push(this._sanitizer.bypassSecurityTrustResourceUrl(res[i].videoCode));
         } 
        },error=>{}
    );

  }

 
}
