import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { webSiteComponent } from './web-site.component';
import { HomeComponent } from './home/home.component';
import { SessionGuard } from '../../services/guards/session-guard.service';
import { OrganizationChartComponent } from './organization-chart/organization-chart.component';
import { SearchComponent } from './search/search.component';
import { SearchDetailsComponent } from './search-details/search-details.component';
import { SearchListViewComponent } from './search-list-view/search-list-view.component';
import { SearchMapViewComponent } from './search-map-view/search-map-view.component';
import { OurCommissionComponent } from './our-commission/our-commission.component';
import { GeneralPublicComponent } from './general-public/general-public.component';
import { FaqDetailsComponent } from './faq-details/faq-details.component';
import { NewsDetailsComponent } from './news-details/news-details.component';
import { CmsPagesComponent } from './cms-pages/cms-pages.component';
import { NewslistComponent } from './news-list/news-list.component';

import { VideoResolver } from 'src/app/shared/resolver/video.resolver';

const routerConfig: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  //{ path: '', component: HomeComponent, resolve:{ videoArr:VideoResolver } },
  {
    path: '',
    component: webSiteComponent,
    children: [
      //{ path: '', loadChildren: () => import('./home/home.module').then(m => m.HomeModule) },
      { path: 'home', component: HomeComponent, resolve:{ videoArr:VideoResolver } }, 
      { path: 'organization-chart', component: OrganizationChartComponent },
      { path: 'search', component: SearchComponent },
      { path: 'search-details', component: SearchDetailsComponent },

      { path: 'search-list-view', loadChildren: () => import('./search-list-view/search-list-view.module').then(m => m.SearchListViewModule) },
      { path: 'search-list-view/:category', loadChildren: () => import('./search-list-view/search-list-view.module').then(m => m.SearchListViewModule) },
      { path: 'search-list-view/:category/:cId', loadChildren: () => import('./search-list-view/search-list-view.module').then(m => m.SearchListViewModule) },
      { path: 'search-list-view/:category/:cId/:keyword', loadChildren: () => import('./search-list-view/search-list-view.module').then(m => m.SearchListViewModule) },

      { path: 'search-map-view', component: SearchMapViewComponent },
      { path: 'search-map-view/:category', component: SearchMapViewComponent },
      { path: 'search-map-view/:category/:cId', component: SearchMapViewComponent },
      { path: 'search-map-view/:category/:cId/:keyword', component: SearchMapViewComponent },
      
      { path: 'our-commission', component: OurCommissionComponent },
      { path: 'general-public', component: GeneralPublicComponent },
      
      { path: 'faq-details', component: FaqDetailsComponent },
      //{ path: 'news-details', component: NewsDetailsComponent },
      { path: 'cms-pages', component: CmsPagesComponent },
      { path: 'pages/:pages', component: CmsPagesComponent },
      { path: 'news/:slug', component: NewsDetailsComponent },
      { path: 'news-list', component: NewslistComponent },
    ],
  //canActivate: [SessionGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routerConfig)
  ],
  exports: [RouterModule],
  providers: [VideoResolver]
})
export class WebsiteRouteModule {
}

