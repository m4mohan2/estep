import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchMapViewComponent } from './search-map-view.component';

describe('SearchMapViewComponent', () => {
  let component: SearchMapViewComponent;
  let fixture: ComponentFixture<SearchMapViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchMapViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchMapViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
