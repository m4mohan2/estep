import { Component, OnInit, OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { NgxUiLoaderService, SPINNER } from 'ngx-ui-loader';
import { AppConst } from 'src/app/app.constants';
import { CommonService } from 'src/app/services/apis/common.service';

import { MatDialog, MatTableDataSource, MatPaginator, PageEvent, MatSort } from '@angular/material';

@Component({
  selector: 'app-search-list-view',
  templateUrl: './search-list-view.component.html',
  styleUrls: ['./search-list-view.component.scss']
})
export class SearchListViewComponent implements OnInit {


  throttle = 150;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = '';

  totalItem: number;
  page: number = 1;
  pageSize:number = 12;
  pageSizeOptions: number[] = [12, 24, 96];

  searchParam: any = {};

  language;
  cityId : string;
  category : string;
  keyword : string;
  searchList :  any = {
      data: []
  }
  showNoData : boolean = false;
  imageUrl : string;
  spinnerType = SPINNER.rectangleBounce;
  constructor(
    private route: ActivatedRoute,
    private UserMasterApi: UserMasterService,
    private router: Router,
    private ngxService :NgxUiLoaderService,
    private common: CommonService,
  ) {
    this.searchList =[];
   }

  ngOnInit() {
    this.route.params.subscribe(params => {
      //console.log("params search-list-view=>", params)
      this.cityId = params['cId'] ? params['cId'] : '';
      this.category = params['category'] ? params['category'] : '';
      this.keyword = params['keyword'] ? params['keyword'] : '';

    })

    this.common.currLang.subscribe(
      res =>{
        this.language = res
      }
    )
  
    this.imageUrl = AppConst.API_BASE_URL + '/upload/entity_logo/';

    this.searchParam.cityId = this.cityId;
    this.searchParam.category = this.category;
    this.searchParam.keyword = this.keyword;
    this.searchParam.page = this.page;
    this.searchParam.pageSize = this.pageSize;
    this.fetchListdata();
  }

  citySrchFunc(data){
    this.cityId = data.city;
    this.category = data.category;
    this.keyword = data.keyword;

    this.searchParam.cityId = this.cityId;
    this.searchParam.category = this.category;
    this.searchParam.keyword = this.keyword;
    this.searchParam.page = this.page;
    this.searchParam.pageSize = this.pageSize;
    this.fetchListdata();
  }

  fetchListdata() {
    this.ngxService.start();
    this.UserMasterApi.getUsersWithApprovedProposalPaging(this.searchParam).subscribe((res) => {
      if (res.success) { 
        this.totalItem = res.data['total'];       
        this.searchList = res.data.data;
        this.ngxService.stop();
      }
    }, error => {
      this.ngxService.stop();
      this.searchList = [];
    });
  }

  getNext(event: PageEvent) {
      console.log('getNext event', event);
      this.page = event.pageIndex + 1;
      this.searchParam.page = event.pageIndex + 1;
      this.searchParam.pageSize = event.pageSize;
      this.fetchListdata();
  }


  gotoDetails(data){
    this.router.navigateByUrl('/search-details', { state: data });
  }

  jsonDecode(item) {
    return JSON.parse(item);
  }

  onScrollDown() {
    console.log('scrolled down!!');
  }

  onUp() {
    console.log('scrolled up!!');
  }

}
