import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from 'src/app/components/components.module';
import { SearchListViewRouteModule } from './search-list-view.routes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from 'src/app/services/apis/auth.service';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { MatSortModule } from '@angular/material';
import { SearchListViewComponent} from './search-list-view.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { OwlDateTimeModule, OwlNativeDateTimeModule,OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';

@NgModule({
  imports: [
    SearchListViewRouteModule,
    SharedModule,
    MatSortModule,
    NgxPaginationModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ComponentsModule
  ],
  declarations: [
    SearchListViewComponent,
  ],
  entryComponents: [
    SearchListViewComponent
  ],
  providers: [
    AuthService, UserMasterService, { provide: OWL_DATE_TIME_LOCALE, useValue: 'en-SG'}
  ]
})
export class SearchListViewModule { }





