import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/services/guards/auth-guard.service';
import { SearchListViewComponent } from './search-list-view.component';
import { NgxUiLoaderModule } from  'ngx-ui-loader';
const routerConfig: Routes = [
  { path: '', component: SearchListViewComponent, pathMatch: 'full' },
];
@NgModule({
  imports: [
    NgxUiLoaderModule,
    RouterModule.forChild(routerConfig)
  ],
  exports: [RouterModule],
  declarations: [],
  providers: [AuthGuard]
})
export class SearchListViewRouteModule { }
