import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DashBordService } from 'src/app/services/apis/dashbord.service';
import { CommonService } from 'src/app/services/apis/common.service';

@Component({
  selector: 'app-general-public',
  templateUrl: './general-public.component.html',
  styleUrls: ['./general-public.component.scss']
})
export class GeneralPublicComponent implements OnInit {
        commissionData:any = []
        commissionParam:any = {};
        language
    constructor(
        private myRoute: Router,
        private dasboardApi:DashBordService,
        private common:CommonService
    ) { 

    }

    ngOnInit() {
        this.common.currLang.subscribe(
            res=>{
                this.language = res
            }
        )
        this.getCmsContent()
        this.gotoTop();
    }
    getCmsContent(){
        this.commissionParam.short_code='about-us'
        this.dasboardApi.getCmsContent(this.commissionParam).subscribe(
            res=>{
                this.commissionData = res
            },
            error=>{

            }
        )
    }

    navigate=(param)=>{
        this.myRoute.navigate([param]);
    }

    gotoTop() {
        window.scrollTo(0, 0);
    }

}
