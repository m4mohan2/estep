import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralPublicComponent } from './general-public.component';

describe('GeneralPublicComponent', () => {
  let component: GeneralPublicComponent;
  let fixture: ComponentFixture<GeneralPublicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralPublicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralPublicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
