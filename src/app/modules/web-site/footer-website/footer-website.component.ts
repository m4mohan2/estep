import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-footer-website',
  templateUrl: './footer-website.component.html',
  styleUrls: ['./footer-website.component.scss']
})
export class FooterWebsiteComponent implements OnInit {

  constructor(private route: Router,) { }

  ngOnInit() {
  }
  
  navigate=(v='')=>{
      this.route.navigateByUrl(v);
  }

}
