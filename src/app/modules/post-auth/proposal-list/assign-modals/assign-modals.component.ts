import { Component, OnInit, Optional, Inject  } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { ProposalService } from 'src/app/services/apis/proposal.service';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { AppConst } from 'src/app/app.constants';
import { TranslateService } from '@ngx-translate/core';
import { NgxUiLoaderService, SPINNER } from 'ngx-ui-loader';
@Component({
  selector: 'app-assign-modals',
  templateUrl: './assign-modals.component.html',
  styleUrls: ['./assign-modals.component.scss']
})
export class AssignModalsComponent implements OnInit {

  spinnerType = SPINNER.rectangleBounce;
  proposalFormAction: FormGroup;
  fileBaseUrl = AppConst.FILE_BASE_URL;

  public f1submitted: boolean = false;

  adminUserlisting;

  fromPage: any;

  constructor(

    public dialogRef: MatDialogRef<AssignModalsComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,

    public router: Router,
    public route: ActivatedRoute,
    private userStorage: UserStorageProvider,
    private toast: ToastProvider,
    private UserMasterApi: UserMasterService,
    private proposalApi: ProposalService,
    private translate: TranslateService,
    private ngxService: NgxUiLoaderService,
  ) 
  {
    this.notesformbuild();

    this.fromPage = data.pageValue;
    this.adminUserlisting = data.adminUserlisting;

    //console.log("fromPage=>", this.fromPage);

    if(this.fromPage.assignUser){
        this.proposalFormAction.controls["assigned_user"].setValue(this.fromPage.assignUser.id);
    }
    localStorage.setItem('proposal_id', this.fromPage.id);
   }
 
  ngOnInit() {
  }

  get f1() { return this.proposalFormAction.controls; };

  notesformbuild() {
    this.proposalFormAction = new FormGroup({
      assigned_user: new FormControl('', [
        Validators.required,
      ]),
    });
  }

  actionSubmit() {

    this.f1submitted = true;
    if (this.proposalFormAction.invalid) { this.toast.error(this.translate.instant('MESSAGE.REQUIRED_MESSAGE')); return; }
    const formData = new FormData();
        
    formData.append('assigned_user',this.proposalFormAction.value.assigned_user);
      //console.log( value); return false;
      this.proposalApi.updateGrant(formData).subscribe(
        (res: any) => {
          localStorage.removeItem('proposal_id');
          if(res.success){
            this.dialogRef.close({success:res.success});
            this.toast.success(this.translate.instant('LOG.ASSIGN')); return;
          }else{
            this.dialogRef.close();
            this.toast.error('Unable to assign'); return;
          }
        },
        (err) => {
          this.f1submitted = false;
        });
  }

  closeDialog() {
    localStorage.removeItem('proposal_id');
    this.dialogRef.close();
  }
  

}
