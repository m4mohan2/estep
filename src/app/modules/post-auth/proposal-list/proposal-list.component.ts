import { AppConst } from 'src/app/app.constants';
import { MatTableDataSource, MatPaginator, PageEvent, MatSort } from '@angular/material';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Component, OnInit, ViewChild  } from '@angular/core';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { ProposalService } from 'src/app/services/apis/proposal.service';
import { ProjectService } from 'src/app/services/apis/project.service';
import { ProposalListingApiResponce } from 'src/app/models/proposal';
import { Router, ActivatedRoute } from '@angular/router';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { NgxUiLoaderService, SPINNER } from 'ngx-ui-loader';
import { CommonService } from 'src/app/services/apis/common.service';
import { HttpHeaders } from '@angular/common/http';
import { ExcelService } from 'src/app/services/apis/excel.service';
import { DatePipe } from '@angular/common';

import { AssignModalsComponent } from './assign-modals/assign-modals.component';
import * as _ from 'lodash';

@Component({
    selector: 'app-proposal-list',
    templateUrl: './proposal-list.component.html',
    styleUrls: ['./proposal-list.component.scss']
})

export class ProposalListComponent implements OnInit {
    spinnerType = SPINNER.rectangleBounce;
    loggedUserType: any;
    loggedUser: any;
    showMessage: string;
    proposallisting: any = {
        data: []
    }

    Statuslisting = [
        {
          key: '1',
          value: 'SUBMIT'
        },
        {
          key: '2',
          value: 'WAITING'
        },
        {
          key: '3',
          value: 'NEEDMOREINFO'
        },
        {
          key: '4',
          value: 'REJECTED'
        },
        {
          key: '5',
          value: 'ACCEPTED'
        },
        {
          key: '6',
          value: 'EVALUATION'
        }
    ]
    date = new Date();
    startDate = this.datePipe.transform(new Date(this.date.getFullYear(), this.date.getMonth(), 1),"yyyy-MM-dd");
    endDate = this.datePipe.transform(new Date(),"yyyy-MM-dd");
    dateTimeRange: Date[];


    viewstatus: boolean;
    editstatus: boolean;
    createtatus: boolean;
    exportstatus: boolean;
    evaluationstatus: boolean;
    assignstatus: boolean;

    assignClickStatus: boolean;
    
    searchParam: any = {};
    fixVar: boolean;
    open: boolean;
    spin: boolean;
    direction: string;
    animationMode: string;
    bydefatseletedproposalType: any;
    proposalType: Array<any>;

    totalItem: number;
    page: number = 1;
    limit: number = 10;
    pageSize:number = 10;
    pageSizeOptions: number[] = [10, 20, 50, 100];


    searchString:string;
    searchStatus:number;
    searchProject:number;
    language;
    loggedUserRole: any;
    downloadParam:any ={}
    excelData;

    displayedColumns = [];
    public saveAssign:boolean;

    adminUserlisting;
    Projectlisting:any =[] ;
    dataSource: MatTableDataSource<any>;
    @ViewChild(MatPaginator , { static: false} ) paginator: MatPaginator;
    @ViewChild(MatSort, { static: false} ) sort: MatSort;
    constructor(
        public router: Router,
        private proposalApi: ProposalService,
        private projectApi: ProjectService,
        private UserMasterApi: UserMasterService,
        private Permission: PermissionProvider,
        private userStorage: UserStorageProvider,
        private ngxService: NgxUiLoaderService,
        private common: CommonService,
        private excelService:ExcelService,
        private datePipe: DatePipe,
        public dialog: MatDialog,
    ) {

        this.loggedUserType = this.userStorage.get().user.user_type;
        this.loggedUser = this.userStorage.get().user;
        //console.log(this.userStorage.get().user)
        this.Projectlisting = [];
        this.proposallisting = [];
        this.showMessage = '';
        this.open = false;
        this.spin = false;
        this.direction = 'up';
        this.proposalType = AppConst.DATA_POTAL_USER_TYPE;
        this.animationMode = 'fling';
        this.bydefatseletedproposalType = 0;

        this.editstatus = Permission.AuthGuardPermission('proposal-edit');
        this.viewstatus = Permission.AuthGuardPermission('proposal-view');
        this.createtatus = true;//Permission.permission('proposal-list', 'create');
        this.exportstatus = true;//Permission.permission('proposal-list', 'export');

        this.evaluationstatus = Permission.permission('proposal-edit', 'evaluation');
        this.assignstatus = Permission.AuthGuardPermission('proposal-assign');
        
        this.saveAssign = false;
        
        this.showMessage = '';
        this.loggedUserRole = this.userStorage.get().user.role.roleKey;

        if(this.loggedUserRole =='system-admin' || this.loggedUserRole =='sub-admin' || this.loggedUserRole =='analysts' || this.loggedUserRole =='analyst-2'){
            this.displayedColumns.push('assignUser');
            //console.log("===>", this.language);
        }

        /*if(this.loggedUserRole =='analysts' || this.loggedUserRole =='analyst-2'){
            this.editstatus = true;
        }*/

        this.getAdminUserList();
    }

    ngOnInit() {
        this.getProjectList()
        this.common.currLang.subscribe(
            res => {
                this.language = res

                if(this.language == 'en'){
                    this.displayedColumns = ['proposal_unique_id', 'entity_name', 'city', 'projectNameEn', 'approved_grant', 'app_status_txt','date', 'updated_at'];
                }else {
                    this.displayedColumns = ['proposal_unique_id', 'entity_name', 'city', 'projectNameEs', 'approved_grant', 'app_status_txt_es','date', 'updated_at'];
                }

                if(this.loggedUserRole =='system-admin' || this.loggedUserRole =='sub-admin' || this.loggedUserRole =='analysts' || this.loggedUserRole =='analyst-2'){
                    this.displayedColumns.push('assignUser');
                }
                
                this.displayedColumns.push('view');
            }
        )

        this.searchParam.language = this.language;
        this.searchParam.page = this.page;
        this.searchParam.pageSize = this.pageSize;

        this.searchParam.user_id = this.userStorage.get().user.id;
        this.searchParam.user_type = this.userStorage.get().user.user_type;
        this.searchParam.myAssigned = this.saveAssign;
        this.fetchProposalListing();
    }

    getProjectList(){
        var projectObj ={
            all : true
        }
        this.projectApi.projectList(projectObj).subscribe((res: any) => {
         
            this.Projectlisting = res;
          
        }, (err) => {});
    }

    fetchProposalListing() {
        this.showMessage = '';
        this.ngxService.start();

        this.proposalApi.listProposal(this.searchParam).subscribe((res: ProposalListingApiResponce) => {
            this.ngxService.stop();
            if (res.success) {

                this.totalItem = res.data['total'];
                this.proposallisting = res.data;

                this.dataSource = new MatTableDataSource(this.proposallisting.data);
                this.dataSource.sort = this.sort;

            }
        }, (err) => {
            this.ngxService.stop();
        });
    }

    getAdminUserList() {
        this.UserMasterApi.getAssignedUser().subscribe((res) => {
          this.adminUserlisting = res.data;
        }, (err) => {
        });
    }

    onChangeStatus(value){
        this.searchParam.status = value;
    }

    onSelectProject(value){
        this.searchParam.project_id = value;
    }

    onChangeAssign(v:boolean){
        console.log(v)
        this.saveAssign = v;
        this.searchParam.myAssigned = this.saveAssign;
    }

    onDateChange(event){
        this.startDate = this.datePipe.transform(event.value[0],"yyyy-MM-dd");
        this.endDate = this.datePipe.transform(event.value[1],"yyyy-MM-dd");
        this.searchParam.startDate = this.startDate
        this.searchParam.endDate = this.endDate
    }

    clear(event) {
        if(event == 'string'){
            this.searchString = null;
            delete this.searchParam.searchString;
        }else if(event == 'status'){
            this.searchStatus = null;
            delete this.searchParam.status;
        }else if(event == 'project'){
            this.searchProject = null;
            delete this.searchParam.project_id;
        }else{
            this.dateTimeRange = null
            delete this.searchParam.startDate;
            delete this.searchParam.endDate;
        }
        delete this.searchParam.export;
        this.fetchProposalListing()
    }

    keyDownFunction(event) {
      if (event.keyCode === 13) {
        //alert('you just pressed the enter key');
        this.proposalSearch();
      }
    }

    proposalSearch() {
        if(this.searchString){
            this.searchParam.searchString = this.searchString;
        }
        delete this.searchParam.export;
        this.searchParam.searchQuery = true;
        this.searchParam.page = 1;
        this.searchParam.pageSize = this.pageSize;
        this.fetchProposalListing();
    }

    getNext(event: PageEvent) {
        this.page = event.pageIndex + 1;
        this.searchParam.page = event.pageIndex + 1;
        this.searchParam.pageSize = event.pageSize;
        this.fetchProposalListing();
    }

    goToLink(id: any, project_id: any) {
        localStorage.setItem('proposal_id', id);
        localStorage.setItem('project_id', project_id);
        this.router.navigate(['post-auth/proposal-edit/' + id]);
    }

    goToView(id: any, project_id: any) {
        localStorage.setItem('proposal_id', id);
        localStorage.setItem('project_id', project_id);
        this.router.navigate(['post-auth/proposal-view/' + id]);
    }

    goToReport(id: any, param: any) {
      
        localStorage.setItem('proposal_id', id);
        this.router.navigate(['post-auth/quaterly-reports/' + id + '/' + param]);
    }

    goToRefund(id: any, project_id: any) {
        localStorage.setItem('proposal_id', id);
        localStorage.setItem('project_id', project_id);
        this.router.navigate(['post-auth/refund/' + id]);
    }

    exportAsXLSX() {
        this.ngxService.start();
        this.searchParam.language = this.language;
        this.searchParam.export = true;
        this.proposalApi.exportProposal(this.searchParam).subscribe(
            res => {
                this.ngxService.stop();
                this.excelData = res.excelData
                this.excelService.exportAsExcelFile(this.excelData, 'proposalList');
            }
        )
    }

    openProposalDialog(row_data) {
        const dialogRef = this.dialog.open(AssignModalsComponent, {
          panelClass: 'dialog-xs',
          disableClose: true,
          width: '600px',
          data: { pageValue: row_data, adminUserlisting: this.adminUserlisting }
        });
        dialogRef.afterClosed().subscribe(response =>{
            //console.log("response=========" ,response)
            if (response.success) {
                this.fetchProposalListing();
            }

        })
      }
    
}