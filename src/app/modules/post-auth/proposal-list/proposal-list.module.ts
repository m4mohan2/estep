import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProposalListRouteModule } from './proposal-list.routes.module';
import { ProposalListComponent } from './proposal-list.component';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { RoleMasterService } from 'src/app/services/apis/role.service';
import { ProjectService } from 'src/app/services/apis/project.service';
import { ProposalService } from 'src/app/services/apis/proposal.service';
import { EcoFabSpeedDialModule } from '@ecodev/fab-speed-dial';
import { MatSortModule } from '@angular/material';
import { NgxPaginationModule } from 'ngx-pagination';
import { OwlDateTimeModule, OwlNativeDateTimeModule,OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
@NgModule({
  imports: [
    ProposalListRouteModule,
    SharedModule,
    EcoFabSpeedDialModule,
    MatSortModule,
    NgxPaginationModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
  ],
  declarations: [
    ProposalListComponent,
  ],
  entryComponents: [
    ProposalListComponent,
  ],

  providers: [UserMasterService, RoleMasterService, ProposalService, ProjectService, { provide: OWL_DATE_TIME_LOCALE, useValue: 'en-SG'}]
})
export class ProposalListModule { }
