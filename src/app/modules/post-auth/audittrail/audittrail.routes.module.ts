import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/services/guards/auth-guard.service';
import { AuditTrailComponent } from './audittrail.component';
import { NgxUiLoaderModule } from  'ngx-ui-loader';
const routerConfig: Routes = [
  { path: '', component: AuditTrailComponent, canActivate: [AuthGuard], pathMatch: 'full' },
];
@NgModule({
  imports: [
    NgxUiLoaderModule,
    RouterModule.forChild(routerConfig)
  ],
  exports: [RouterModule],
  declarations: [],
  providers: [AuthGuard]
})
export class AuditTrailRouteModule { }
