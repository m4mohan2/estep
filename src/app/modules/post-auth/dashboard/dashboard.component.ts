import { Component, OnInit } from '@angular/core';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { AppConst } from 'src/app/app.constants';
import { DashBordService } from 'src/app/services/apis/dashbord.service';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { DashbordApiResponce,AdminDashboardGraphApiResponce } from 'src/app/models/dashbord';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { NgxUiLoaderService, SPINNER } from 'ngx-ui-loader';
import { CommonService } from 'src/app/services/apis/common.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    providers: [DashBordService, ToastProvider, DatePipe],
    
})

export class DashboardComponent implements OnInit {
    public owlItems: any;
    public barchartObj: any;
    selectedIndex:number = 0;
    public StaticColor: any;
    viewBarChart: boolean;
    public from: any;
    public to: any;
    bydefatseletedUserType: any;
    public datacount: number;
    public graphtag: any;
    chartParam:any ={}
    date = new Date();
    startDate = this.datePipe.transform(new Date(this.date.getFullYear(), this.date.getMonth(), 1),"yyyy-MM-dd");
    endDate = this.datePipe.transform(new Date(),"yyyy-MM-dd");
    monthLastDate = this.endOfMonth(this.startDate);
    type;
    dateTimeRange: Date[];
    reportParam:any ={};
    title;
    titleEs;
    barChart:boolean = false;
    imgUrl: any;
    spinnerType = SPINNER.rectangleBounce;
    language
    constructor(
        private consoleProvider: ConsoleProvider,
        private DashbordApi: DashBordService,
        private toast: ToastProvider,
        private datePipe: DatePipe,
        private router: Router,
        private userStorage: UserStorageProvider,
        private Permission: PermissionProvider,
        private ngxService :NgxUiLoaderService,
        private common : CommonService
    ) {
        this.imgUrl = AppConst.IMG_BASE_URL;
        this.viewBarChart = Permission.permission('Bar Chart', 'view');
        this.graphtag = '';
        this.StaticColor = ['#5271ba', '#3da3e8', '#51c0bf', '#fbb03b', '#f47b50',
        '#5271ba', '#3da3e8', '#51c0bf', '#fbb03b', '#f47b50',
        '#5271ba', '#3da3e8', '#51c0bf', '#fbb03b', '#f47b50',
        '#5271ba', '#3da3e8', '#51c0bf', '#fbb03b', '#f47b50',
        '#5271ba', '#3da3e8', '#51c0bf', '#fbb03b', '#f47b50',
        '#5271ba', '#3da3e8', '#51c0bf', '#fbb03b', '#f47b50',
        ];
        this.barchartObj = {};
    }

    ngOnInit() {
        this.common.currLang.subscribe(
            res=>{
                this.language =res
            }
        )
        this.dateTimeRange= [new Date(this.startDate), this.monthLastDate];
        this.chartParam.startDate = this.startDate
        this.chartParam.endDate = this.endDate
        this.chartParam.type = this.type
        this.reportParam.startDate = this.startDate
        this.reportParam.endDate = this.endDate
        this.getDashbordaData();
        this.getChartData();
    }

    getDashbordaData() {
        this.DashbordApi.getreportTab(this.reportParam).subscribe((res: DashbordApiResponce) => {
            this.owlItems = res['response'];
            //console.log(this.chartParam)
            if(this.chartParam.type != undefined){
                for(let i = 0;i <  this.owlItems.length;i++){
                    if(this.owlItems[i].Title == this.chartParam.type ){
                        this.title = this.owlItems[i].Name
                        this.titleEs = this.owlItems[i].NameEs
                    }
                }
            }else{
                this.title = res['response'][0].Name
                this.titleEs = res['response'][0].NameEs
            }

        }, (err) => {
        });
    }

    getChartData(){
        this.ngxService.start()
        this.DashbordApi.getGraphData( this.chartParam).subscribe((res: AdminDashboardGraphApiResponce) => {
            if (res.success) {
                this.ngxService.stop();
                var dateList = [];
                var dataList = [];
                for(var i=0; i<res.message.length; i++) {
                    dateList.push(res.message[i]['date']);
                    dataList.push(res.message[i]['total']);
                }
                this.setBarChart(dateList, dataList);
                this.datacount = 0;
                dataList.map((item, index) => {
                    this.datacount = this.datacount + item;
                });
            }
        }, (err) => {
        });
    }

    setBarChart(label?, datas?) {
        this.barChart =  true;
        this.barchartObj.barChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true,
            legend: false,
            scales: {
                xAxes: [{
                    barPercentage: 1,
                    barThickness: 25,
                    maxBarThickness: 35,
                    minBarLength: 2,
                    gridLines: {
                        offsetGridLines: false,
                        display: false
                    }
                }]
            }
        };
        this.barchartObj.barChartLabels = label ? label : [];
        this.barchartObj.barChartType = 'bar';
        this.barchartObj.barChartLegend = true;
        this.barchartObj.barChartData = [
            {
                data: datas ? datas : [],
                backgroundColor: this.StaticColor,
                hoverBackgroundColor: this.StaticColor
            }
        ];
    }

    onClickowlItems(index:number,item){
        this.title = item.Name
        this.titleEs = item.NameEs
        
        this.selectedIndex = index;
        this.chartParam.type = item.Title
        this.getChartData()
    }

    onDateChange(event){

        console.log(event);
        
        this.startDate = this.datePipe.transform(event.value[0],"yyyy-MM-dd");
        this.endDate = this.datePipe.transform(event.value[1],"yyyy-MM-dd");
        this.chartParam.startDate = this.startDate
        this.chartParam.endDate = this.endDate
        
        this.getChartData()
        this.reportParam.startDate = this.startDate
        this.reportParam.endDate = this.endDate
        this.getDashbordaData()
    }

    clearValue(event) {
        this.dateTimeRange = [new Date(this.startDate), this.monthLastDate]
        // this.chartParam.startDate = ''
        // this.chartParam.endDate = ''
        // this.reportParam.startDate = ''
        // this.reportParam.endDate = ''
        this.chartParam.startDate = this.datePipe.transform(new Date(this.date.getFullYear(), this.date.getMonth(), 1),"yyyy-MM-dd");
        this.chartParam.endDate = this.datePipe.transform(new Date(),"yyyy-MM-dd");
        
        this.getChartData()
        this.reportParam.startDate = this.datePipe.transform(new Date(this.date.getFullYear(), this.date.getMonth(), 1),"yyyy-MM-dd");
        this.reportParam.endDate = this.datePipe.transform(new Date(),"yyyy-MM-dd");
        this.getDashbordaData()
    }

    endOfMonth(date){
        return new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);
    }

}
