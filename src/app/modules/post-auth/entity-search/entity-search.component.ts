import { Component, OnInit } from '@angular/core';
import { EntitysearchService } from 'src/app/services/apis/entitysearch.service';
import { Router } from '@angular/router';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { NgxUiLoaderService,SPINNER } from 'ngx-ui-loader';
import { CommonService } from 'src/app/services/apis/common.service';
import { ExcelService } from 'src/app/services/apis/excel.service';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';

@Component({
	selector: 'app-entity-search',
	templateUrl: './entity-search.component.html',
	styleUrls: ['./entity-search.component.scss']

})

export class EntitySearchComponent implements OnInit {
	userlist: Array<any>;
	searchString: string;
	length;
	
    page: number = 1;
    limit:number =10
	itemsPerPage:number = 10;

	searchParam: any = {};
	spinnerType = SPINNER.rectangleBounce;
	language;
	excelData:any =[];
	loggedUserType: any;
	constructor(
		private entitysearchApi: EntitysearchService,
		public router: Router,
		private toast: ToastProvider,
		private ngxService:NgxUiLoaderService,
		private common:CommonService,
		private excelService:ExcelService,
		private userStorage: UserStorageProvider
	) {
		this.userlist = [];
		this.searchString = '';
		this.loggedUserType = this.userStorage.get().user.user_type;
	}

	ngOnInit() {
		this.common.currLang.subscribe(
			res=>{
				this.language =res
			}
		)
		this.searchParam.page = this.page;
		//this.searchParam.language = this.language;
		this.searchParam.export = 0;

		this.fetchUserList();
	}

	clear(event) {
        if(event == 'string'){
            this.searchString = null;
            delete this.searchParam.searchString;
        }
        delete this.searchParam.export;
        this.searchParam.page = this.page = 1;
        this.fetchUserList()
    }

	newSearch() {
		delete this.searchParam.page;
		this.searchParam.page = this.page = 1;

		if(this.searchString){
            this.searchParam.searchString = this.searchString;
        }

		this.fetchUserList();
	}

	fetchUserList = () => {
		this.ngxService.start();

		this.entitysearchApi.userlist(this.searchParam).subscribe((res: any) => {
			//console.log(res);
			this.ngxService.stop()
			this.userlist = res.data;
			this.length = res['total'];
		}, (err) => {
			this.ngxService.stop()
		});
	}

	paginationFeed(page){
        this.page = page;
        let start = (page - 1) * this.itemsPerPage;
        this.searchParam.page = page;
        this.fetchUserList();
	}

	exportAsXLSX() {
        this.searchParam.language = this.language;
        this.searchParam.export = 1;
        this.entitysearchApi.userlist(this.searchParam).subscribe(
            res => {
                this.excelData = res.excelData
                this.excelService.exportAsExcelFile(this.excelData, 'entityList');
            }
        )
    }
	

}


