import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';

@Component({
  selector: 'app-entity-info',
  templateUrl: './entity-info.component.html',
  styleUrls: ['./entity-info.component.scss']
})
export class EntityInfoComponent implements OnInit {
  loggedUserRole: any;
  proposalid: number;
  constructor(
    
    private dialogRef: MatDialogRef<EntityInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public showData:any,
    public router: Router,
    public route: ActivatedRoute,
    private userStorage: UserStorageProvider,
    private toast: ToastProvider,
  ) 
  {
    this.loggedUserRole = this.userStorage.get().user.role.roleKey;
   }
 
  ngOnInit() {
    //console.log("data",showData)
    //console.log(this.userStorage.get());
  }
  


  
  

}
