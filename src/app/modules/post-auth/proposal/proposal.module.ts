import { NgModule } from '@angular/core';
import { ProposalRouteModule } from './proposal.routes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from 'src/app/services/apis/auth.service';
import { ProposalService } from 'src/app/services/apis/proposal.service';
import { ProjectService } from 'src/app/services/apis/project.service';
import { ProposalComponent,
  RejectedModalComponent,
  ApprovedModalComponent, 
  ReprogramModalComponent, 
  RequestModalComponent, 
  RejectGrantModalComponent, 
  PaidModalComponent,
  WatingModalComponent
  } from './proposal.component';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { NotesService } from 'src/app/services/apis/notes.service';
import { MoreinfoService } from 'src/app/services/apis/moreinfo.service';
import { TruncateModule } from '@yellowspot/ng-truncate';
import { NgxCurrencyModule } from "ngx-currency";
import { AuditTrailService } from 'src/app/services/apis/audittrail.service';

@NgModule({
  imports: [
    ProposalRouteModule,
    SharedModule,
    MatSortModule,
    TruncateModule,
    NgxCurrencyModule
  ],
  declarations: [
    ProposalComponent,
    ApprovedModalComponent,
    WatingModalComponent,
    RejectedModalComponent,
    ReprogramModalComponent,
    RequestModalComponent,
    RejectGrantModalComponent,
    PaidModalComponent,
    WatingModalComponent
  ],
  entryComponents: [
    ApprovedModalComponent,
    RejectedModalComponent,
    ReprogramModalComponent,
    RequestModalComponent,
    RejectGrantModalComponent,
    PaidModalComponent,
    WatingModalComponent,
  ],
  providers: [AuthService, UserMasterService, ProposalService, ProjectService, NotesService, MoreinfoService, AuditTrailService]
})
export class ProposalModule { }
