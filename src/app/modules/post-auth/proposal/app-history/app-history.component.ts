import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MoreinfoService } from 'src/app/services/apis/moreinfo.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';

@Component({
  selector: 'app-app-history',
  templateUrl: './app-history.component.html',
  styleUrls: ['./app-history.component.scss']
})
export class AppHistoryComponent implements OnInit {
  loggedUserRole: any;
  moreinfolisting: Array<any>;
  addMoreinfoform: FormGroup;
  proposalid: number;
  public f1submitted: boolean = false;
  constructor(
    
    private dialogRef: MatDialogRef<AppHistoryComponent>,
    @Inject(MAT_DIALOG_DATA) public showData:any,
    private moreinfoApi: MoreinfoService,
    public router: Router,
    public route: ActivatedRoute,
    private userStorage: UserStorageProvider,
    private toast: ToastProvider,
  ) 
  {
    //this.moreinfoformbuild();
    this.moreinfolisting = [];
    this.loggedUserRole = this.userStorage.get().user.role.roleKey;
   }
 
  ngOnInit() {
    //console.log(this.userStorage.get());
    //this.getmoreinfolist(localStorage.getItem('proposal_id'));
    this.moreinfolisting = this.userStorage.getHistory();
  }
 
  
  
  


}
