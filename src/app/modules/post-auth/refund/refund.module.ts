import { NgModule } from '@angular/core';
import { RefundRouteModule } from './refund.routes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from 'src/app/services/apis/auth.service';
import { RefundComponent } from './refund.component';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { RefundService } from 'src/app/services/apis/refund.service';
import { UserEditService } from 'src/app/services/apis/useredit.service';
import { NgxCurrencyModule } from "ngx-currency";

@NgModule({
  imports: [
    RefundRouteModule,
    SharedModule,
    MatSortModule,
    NgxCurrencyModule
  ],
  
  declarations: [
    RefundComponent,
  ],
  entryComponents: [
    RefundComponent,
  ],
  providers: [AuthService, UserMasterService, RefundService, UserEditService]
})
export class RefundModule{

 
 }
