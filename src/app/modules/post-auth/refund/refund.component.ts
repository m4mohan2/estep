import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { RefundService } from 'src/app/services/apis/refund.service';
import { CommonService } from 'src/app/services/apis/common.service';
import { DatePipe } from '@angular/common';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { error } from 'util';
import { NgxUiLoaderService, SPINNER } from 'ngx-ui-loader';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-refund',
    templateUrl: './refund.component.html',
    styleUrls: ['./refund.component.scss']
})
export class RefundComponent implements OnInit {
    public refundData: any;
    public refundForm: FormGroup;


    spinnerType = SPINNER.rectangleBounce;
	filevaidation: boolean;
	showLoader: boolean;
    DocMessage: string;
    newslistid: number;
    quaterlyReportform: FormGroup;
    f1submitted: boolean;
    p: number = 1;
    language: string;
    newsParam: any = {};
    msg;
    created_at = new Date(1990, 0, 1);
    successMsg: boolean = false;
    errorMsg: boolean = false;
    userid: number;
    filesToUpload: Array<File> = [];
    imageUrl: Array<any> = [];
    upFiles: any = [];
    allfiles: any = [];
    proposalid: any;
    proposal_unique_id: any;
    loggedUserType: any;
    classificationData:any = [];
    evidenceData:any =[]
	fileUrl:string = environment.file_base_url
    loadingFile:boolean = false;
    
    RefundList: any = [];
    
    constructor(
        private fb: FormBuilder,
        private toast: ToastProvider,
        private refundApi: RefundService,
        private common: CommonService,
        public router: Router,
		private route: ActivatedRoute,
        public datePipe: DatePipe,
        private userStorage: UserStorageProvider,
        private ngxService: NgxUiLoaderService,
        private sanitizer: DomSanitizer,
        private translate: TranslateService,
    ) {
        this.newsformbuild();
        this.filevaidation = false;
        this.DocMessage = '';
		this.newslistid = 0;
        this.showLoader = false;
        this.loggedUserType = this.userStorage.get().user.user_type;
    }
    ngOnInit() {
        this.newsParam.page = this.p;

        this.common.currLang.subscribe(
            res => {
                this.language = res;
            }
        )
        this.route.params.subscribe(params => {
            if (params) {
                //console.log(params);
                if (params['id']) {
                    //console.log(params['id']);
                    this.proposalid = +params['id'];
                    if (this.proposalid > 0) {
                        this.getuserdetaiils();
                    }
				}
            }
        });
        this.getRefundList();

        this.refundForm = this.fb.group({
              expenditure_desc: ['', Validators.required],
              check_no: ['', Validators.required],
              infavourof: ['', Validators.required],
              expense_date: [ new Date(), ''],
              expense_amount: ['', Validators.required],
              refund_amount: ['', ''],
              refund_status: ['', ''],
        });

        if(this.loggedUserType===0) {
            this.refundForm.get('refund_amount').setValidators(Validators.required);
            this.refundForm.get('refund_status').setValidators(Validators.required);
        } else {
            this.refundForm.get('refund_status').clearValidators();
            this.refundForm.get('refund_status').clearValidators();
        }
        
    }

    getRefundList(){
        this.ngxService.start()
        this.refundApi.refundList(this.proposalid).subscribe(
            res=>{
                //console.log(res)
                this.ngxService.stop()
                this.RefundList = res['data']['data'];
            }
        )
    }
    
    newsformbuild() {
        this.quaterlyReportform = new FormGroup({
            entity_name: new FormControl(''),
            entity_code: new FormControl(''),
            created_at: new FormControl(''),
            proposal_unique_id: new FormControl(''),
            user_name: new FormControl(''),
        });
    }

    goToLink(id: any) {
        localStorage.setItem('refund_id', id);
        this.getRefund();
    }

    get f1() { return this.refundForm.controls; };

    getuserdetaiils = () => {
        this.refundApi.qualityreports(this.proposalid).subscribe(
            res => {
                this.quaterlyReportform.patchValue({
                    id: this.proposalid,
                    entity_name: res.data[0].entity_name,
                    entity_code: res.data[0].entity_code,
                    created_at: this.datePipe.transform(res.data[0].created_at,"MM-dd-yyyy"),
					proposal_unique_id: res.data[0].proposal_unique_id,
				});

                this.proposal_unique_id = res.data[0].proposal_unique_id;
				
				if(res.data[0].db_c2 === null && res.data[0].db_c3 === null) {
					this.quaterlyReportform.patchValue({
						db_c2:0,
						db_c3:0
					});
				}
            }
        )
    }

    getRefund() {
        this.refundApi.viewRefund().subscribe((res: any) => {
              if (res.success) {
                  this.refundData = res.data;

                this.refundForm.controls["expenditure_desc"].setValue(this.refundData.expenditure_desc);
                this.refundForm.controls["expense_date"].setValue(this.datePipe.transform(this.refundData.expense_date,"yyyy-MM-dd"));
                this.refundForm.controls["check_no"].setValue(this.refundData.check_no);
                this.refundForm.controls["expense_amount"].setValue(this.refundData.expense_amount);
                this.refundForm.controls["infavourof"].setValue(this.refundData.infavourof);
                
                if(this.loggedUserType===0) { // admin
                    this.refundForm.controls["refund_amount"].setValue(this.refundData.refund_amount);
                    this.refundForm.controls["refund_status"].setValue(this.refundData.refund_status);
                }
            }
        }, (err) => {});
    }

    onSubmit(value){
        this.f1submitted = true;
        var addParam={
            proposal_id:this.proposalid, 
            proposal_unique_id:this.proposal_unique_id, 
            expenditure_desc:value.expenditure_desc, 
            expense_date:this.datePipe.transform(value.expense_date,"yyyy-MM-dd"), 
            check_no:value.check_no, 
            expense_amount:value.expense_amount, 
            infavourof:value.infavourof,
            refund_amount:value.refund_amount,
            refund_status:value.refund_status
        }
        if (this.refundForm.invalid) { this.toast.error(this.translate.instant('MESSAGE.REQUIRED_MESSAGE')); return; }
        this.ngxService.start()
        this.refundApi.addRefund(addParam).subscribe(
            res=>{
                this.f1submitted = false;
                if(res.update){
                    localStorage.removeItem('refund_id');
                }
                if(res.success){
                    this.ngxService.stop()
                    this.getRefundList()
                    this.refundForm.reset()
                    this.msg = res['message'];
                    this.toast.success(this.msg) 
                }
            },
            error=>{
                this.ngxService.stop()
                this.msg = error['message'];
                this.toast.error(this.msg) 
            }
        )
    }
    onClickDeleteRefund(event){
        this.refundApi.deleteRefund(event).subscribe(
            res=>{
                if(res.success){
                    this.getRefundList()
                    this.getuserdetaiils()
                    this.msg = res['message'];
                    this.toast.success(this.msg) 
                }
            },
            error=>{
                this.msg = error['message'];
                this.toast.error(this.msg) 
            }
        )
    }
    
    onActivate(event) {
        window.scroll(0, 0);
    }



}
