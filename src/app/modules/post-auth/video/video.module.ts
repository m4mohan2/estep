import { NgModule } from '@angular/core';
import { VideoRouteModule } from './video.routes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from '../../../services/apis/auth.service';
import { VideoComponent } from './video.component';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { CKEditorModule } from 'ngx-ckeditor';
import { VideoService } from '../../../services/apis/video.service';


@NgModule({
  imports: [
    VideoRouteModule,
    SharedModule,
    MatSortModule,
    CKEditorModule

  ],
  declarations: [
    VideoComponent,
  ],
  entryComponents: [
    VideoComponent,
  ],
  providers: [AuthService, UserMasterService,VideoService ]
})
export class VideoModule { }
