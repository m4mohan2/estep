import { Component, OnInit, ViewChild } from '@angular/core';
import { VideoService } from '../../../services/apis/video.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { UserStorageProvider } from '../../../services/storage/user-storage.service';
import { AppConst } from 'src/app/app.constants';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {
  @ViewChild('fileInput', { static: false} ) fileInput;
  public editorValue: string = '';
  addVideoform: FormGroup;
  msg;
  successMsg: boolean = false;
    errorMsg: boolean = false;
    DocMessage: string;
    userid: number;
  videolistid: number;
  filevaidation: boolean;
  public f1submitted: boolean = false;
  filesToUpload: Array<File> = [];
  imageUrl: Array<any> = [];
  upFiles: any = [];
  allfiles: any = [];
  constructor(
    private videoApi: VideoService,
    public router: Router,
    public route: ActivatedRoute,
    private userStorage: UserStorageProvider,
    private toast: ToastProvider,
  ) {
    this.userid = 1;
    this.videoformbuild();
    this.filevaidation = false;
    this.DocMessage = '';
    this.videolistid = 0;
  }



  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params) {
        //console.log(params);
        if (params['id']) {
          //console.log(params['id']);
          this.videolistid = +params['id'];
          if (this.videolistid > 0) {
            this.view();
          }
        }
      }
    });

  }
  get f1() { return this.addVideoform.controls; };
  videoformbuild() {
    this.addVideoform = new FormGroup({

      videoCode: new FormControl('', [
        Validators.required,
      ]),

      descriptions: new FormControl('', [
        Validators.required,
      ]),
      descriptionsSpanish: new FormControl('', [
        Validators.required,
      ]),

     

      status: new FormControl('', [
        Validators.required,
      ]),

      UpdatedBy: new FormControl(this.userStorage.get().user.id ? this.userStorage.get().user.id : '', [
      ]),
      CreatedBy: new FormControl(this.userStorage.get().user.id ? this.userStorage.get().user.id : '', [
      ]),


    });
  }


addVideo(value) {
  //console.log(value)
    this.f1submitted = true;
    //const files: Array<File> = this.filesToUpload;
    const files: Array<File> = this.allfiles;
    if (this.videolistid > 0) {
      const formData = new FormData();
      // tslint:disable-next-line:prefer-for-of
      
      formData.append('videoCode', value.videoCode);   
      formData.append('descriptions', value.descriptions);
      formData.append('descriptionsSpanish', value.descriptionsSpanish);
      formData.append('status', value.status);
      formData.append('UpdatedBy', value.UpdatedBy);
      this.videoApi.editvideo(formData, this.videolistid).subscribe(
        (res: any) => {
          //console.log('==' + res);
          //this.successMsg  = true ; 
          this.msg=res['message'];
          this.router.navigate(['post-auth/video-list']);
          this.toast.success( this.msg)
        },
        (err) => {
          this.f1submitted = false;
        });
    } else {
      //console.log(this.addVideoform)
      if (this.addVideoform.invalid) { this.toast.error('Please enter the required fields !!'); return; }
      //console.log( value)
      const formData = new FormData();
      // tslint:disable-next-line:prefer-for-of
     
        
      formData.append('videoCode', value.videoCode);
      formData.append('descriptions', value.descriptions);
      formData.append('descriptionsSpanish', value.descriptionsSpanish);
      formData.append('status', value.status);
      formData.append('CreatedBy', value.CreatedBy);
      formData.append('UpdatedBy', value.UpdatedBy);
      
      this.videoApi.addvideo(formData).subscribe(
        (res: any) => {
          //this.successMsg  = true ; 
          this.msg=res['message'];
          this.router.navigate(['post-auth/video-list']);
          this.toast.success( this.msg)
          this.addVideoform.reset();
          setTimeout(() =>{
            this.errorMsg  = false;
           
         }, 3000); 
         

       //this.router.navigate(['post-auth/video-list']);
        },
        (err) => {
          this.f1submitted = false;
          this.msg=err['message'];
          this.toast.error( this.msg)
        });
    }



  }



  view() {

    //console.log(this.id);
    this.videoApi.view(this.videolistid).subscribe(
      data => {
        //console.log(data);
        
        if(this.videolistid){
          this.addVideoform.setValue({
            videoCode: data.videoCode,
            descriptions: data.descriptions,
            descriptionsSpanish: data.descriptionsSpanish,
            status: data.status,
            UpdatedBy: this.userStorage.get().user.id ? this.userStorage.get().user.id : '',
            CreatedBy:''
            
          }
          );
        }else{

          this.addVideoform.setValue({
            videoCode: data.videoCode,
            descriptions: data.descriptions,
            descriptionsSpanish: data.descriptionsSpanish,
            status: data.status,
            CreatedBy: this.userStorage.get().user.id ? this.userStorage.get().user.id : '',
            UpdatedBy:''
          }
          );
        }
        
        this.addVideoform.updateValueAndValidity();
        //console.log(this.addVideoform);
      }
    )

  }
  
  onActivate(event) {
    window.scroll(0,0);
  }

 
}     
