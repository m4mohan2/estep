import { AppConst } from 'src/app/app.constants';
import { Component, OnInit, Inject, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSelectChange } from '@angular/material';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { RoleMasterService } from 'src/app/services/apis/role.service';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { equalvalidator } from 'src/app/directives/validators/equal-validator';


@Component({
  selector: 'app-edit-user-admin',
  templateUrl: './edit-user-admin.component.html',
  styleUrls: ['./edit-user-admin.component.scss'],
  providers: []
})
export class EditUserAdminComponent implements OnInit {
  editAdminUserform: FormGroup;
  showLoader: boolean;
  RoleListing: Array<any>;
  StateList: Array<any>;
  Citylisting: Array<any>;
  statevalidation: boolean;
  @Output() OnEditAdmin = new EventEmitter<any>(true);
  constructor(
    private formBulder: FormBuilder,
    private toast: ToastProvider,
    private dialogRef: MatDialogRef<EditUserAdminComponent>,
    private consoleProvider: ConsoleProvider,
    private RoleApi: RoleMasterService,
    private UserMasterApi: UserMasterService,
    @Inject(MAT_DIALOG_DATA) public data: any

  ) {
    // console.log(JSON.stringify(this.data));
    this.RoleListing = [];
    this.StateList = [];
    this.statevalidation = false;
    this.Citylisting = [];
    this.fetchRoleListing();
    this.getStateList().then(() => {
      this.SetCity(this.data.admin_state);
    });
  }
  /*
  * function build form with exting data
  * @param
  * @memberof EditUserAdminComponent
  */
  ngOnInit() {
    //console.log(this.data.activeStatus.toString(2))
    /* tslint:disable */
    const emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    /* tslint:enable */
    this.editAdminUserform = new FormGroup({
      id: new FormControl(this.data.id, [
      ]),
      roleId: new FormControl(this.data.hasOwnProperty('roleId') ? this.data.roleId ? this.data.roleId : '' : '', [
        Validators.required,
      ]),
      email: new FormControl(this.data.hasOwnProperty('email') ? this.data.email : '', [ ]),
      admin_first_name: new FormControl(this.data.hasOwnProperty('admin_first_name') ? this.data.admin_first_name : '', [
        Validators.required,
      ]),
      admin_last_name: new FormControl(this.data.hasOwnProperty('admin_last_name') ? this.data.admin_last_name : '', [
        Validators.required,
      ]),
      admin_phone: new FormControl(this.data.hasOwnProperty('admin_phone') ? this.data.admin_phone : '', [
        Validators.required
      ]),
      admin_address: new FormControl(this.data.hasOwnProperty('admin_address') ? this.data.admin_address : '', [
        Validators.required
      ]),
      admin_state: new FormControl(this.data.hasOwnProperty('admin_state') ? this.data.admin_state : '', [
        Validators.required,
      ]),
      admin_city: new FormControl(this.data.hasOwnProperty('admin_city') ? this.data.admin_city : '', [
        Validators.required,
      ]),
      password: new FormControl('', []),
      confirmPassword: new FormControl('', [equalvalidator('password')]),
      activeStatus: new FormControl(this.data.activeStatus.toString(2), [
        Validators.required,
      ])
    });
  }
  getStateList() {
    return new Promise((resolve, reject) => {
      this.UserMasterApi.getState().subscribe((res: any) => {
        if (res.success) {
          this.StateList = res.data;
          resolve();
        }
      }, (error) => {
        console.log(error);
        reject();
      });
    });
  }
  StateChange(event) {
    const index = this.StateList.findIndex(x => x.id === event.value);
    if (index > -1) {
      this.Citylisting = this.StateList[index].cities;
    }
  }
  SetCity(value) {
    const index = this.StateList.findIndex(x => x.id === value);
    if (index > -1) {
      this.Citylisting = this.StateList[index].cities;
    }
  }
  /*
  * function use ferch  role for admin user
  * @param
  * @memberof EditUserAdminComponent
  */
  fetchRoleListing() {
    this.showLoader = true;
    this.RoleApi.rolelisting().subscribe((response: any) => {
       //console.log(response);
      this.showLoader = false;
      if (response.resCode === 200) {
        this.RoleListing = response.data;
      }
    }, (errorData: any) => {
      this.showLoader = false;
    });
  }
  /*
  * function use to api call  edit admin user
  * @param
  * @memberof EditUserAdminComponent
  */
  editUsertype(userInfo) {
    if (this.editAdminUserform.valid) {
      this.showLoader = true;
      this.UserMasterApi.AdminUserUpdate(userInfo).subscribe((response: any) => {
        this.showLoader = false;
        // console.log(response);
        if (response.success) {
          this.OnEditAdmin.emit(response);
          this.dialogRef.close();
          this.toast.success('Admin User Updated Successfully');
        }
      }, (errorData: any) => {
        this.showLoader = false;
      });
    }
  }
}
