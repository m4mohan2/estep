import { NgModule } from '@angular/core';
import { UserEditRouteModule } from './user-editroutes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from 'src/app/services/apis/auth.service';
import { UserEditComponent } from './user-edit.component';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { CKEditorModule } from 'ngx-ckeditor';
import { RegistrationService } from 'src/app/services/apis/registration.service';
import { UserEditService } from 'src/app/services/apis/useredit.service';
import { NgxPrintModule} from 'ngx-print';

import { SidenavService } from 'src/app/services/side-nav.service';

@NgModule({
  imports: [
    UserEditRouteModule,
    SharedModule,
    MatSortModule,
    CKEditorModule,
    NgxPrintModule

  ],
  declarations: [
    UserEditComponent,
    
  ],
  entryComponents: [
    UserEditComponent,
    
  ],
  providers: [AuthService, UserMasterService,RegistrationService,UserEditService,SidenavService]
})
export class UserEditModule{ }
