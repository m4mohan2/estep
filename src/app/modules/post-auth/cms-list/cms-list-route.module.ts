import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/services/guards/auth-guard.service';
import { NgxUiLoaderModule } from  'ngx-ui-loader';
import { CmsListComponent } from './cms-list.component';
const routerConfig: Routes = [
  { path: '', component: CmsListComponent, canActivate: [AuthGuard], pathMatch: 'full' },
];
@NgModule({
  imports: [
    NgxUiLoaderModule,
    RouterModule.forChild(routerConfig)
  ],
  exports: [RouterModule],
  declarations: [],
  providers: [AuthGuard]
})
export class CmsListRouteModule { }
