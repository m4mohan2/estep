import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/services/guards/auth-guard.service';
import { NewsListComponent } from './news-list.component';
import { NgxUiLoaderModule } from  'ngx-ui-loader';
const routerConfig: Routes = [
  { path: '', component: NewsListComponent, canActivate: [AuthGuard], pathMatch: 'full' },
];
@NgModule({
  imports: [
    NgxUiLoaderModule,
    RouterModule.forChild(routerConfig)
  ],
  exports: [RouterModule],
  declarations: [],
  providers: [AuthGuard]
})
export class NewsListRouteModule { }
