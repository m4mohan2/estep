import { Component, OnInit, Inject } from '@angular/core';
import { VideoService } from '../../../services/apis/video.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { NgxUiLoaderService,SPINNER } from 'ngx-ui-loader';
import { CommonService } from 'src/app/services/apis/common.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
 
})

export class VideoListComponent implements OnInit {
	videoliting: Array<any>;
	videolist:Array<any> = [];
	length;
    msg;
    p: number = 1;
    limit:number =10
	itemsPerPage:number = 10;
	videoParam: any = {};
	spinnerType = SPINNER.rectangleBounce;
	createtatus: boolean;
  	editstatus: boolean;
	deletestatus: boolean;
	searchString; 
	language: string;
	constructor(
		private videoApi: VideoService,
		public router: Router,
		public dialog: MatDialog,
		private toast: ToastProvider,
		private Permission: PermissionProvider,
		private ngxService:NgxUiLoaderService,
		private common: CommonService,
		public  _sanitizer: DomSanitizer
	) {
		this.videoliting = [];
		this.editstatus = Permission.sidePanelpermission('video-edit');
	    this.createtatus = Permission.sidePanelpermission('video-add');
		this.deletestatus = Permission.permission('video-list', 'delete');
	}

	ngOnInit() {
		this.videoParam.page = this.p;
        //this.videoParam.limit = this.limit;
		this.common.currLang.subscribe(
            res => {
                this.language = res;
                this.getvideolist();
            }
        )
	}

	getvideolist = () => {
		this.ngxService.start();
		this.videoApi.video(this.videoParam).subscribe((res: any) => {
			this.ngxService.stop();
			//console.log(res);
			this.videoliting = res.data;
			this.length = res['total'];
			this.videoliting = res.data;

			this.videoliting = res.data;
			for (let i = 0; i < res.data.length; i++) {		
				this.videoliting[i].videoCode = this._sanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/"+res.data[i].videoCode);			
					   
			  }

		}, (err) => {
			this.ngxService.stop()
		});
	}

	videodelete(id) {
		const dialogRef = this.dialog.open(VideoDeleteModalComponent, {
			panelClass: 'dialog-xs',
			data:{status:'video-delete'},
			width: '600px'
		});
		dialogRef.afterClosed().subscribe(response =>{
			//console.log(response)
			if(response.delStatus == 'deleted'){
			  //console.log('hi')
				this.videoApi.delete(id).subscribe((res: any) => {
					//console.log(res);
					this.msg = res['message']
					this.toast.success( this.msg)
					this.getvideolist();
				}, (err) => {
					this.msg = err['message']
					this.toast.error( this.msg)
				});
			}
	  
		})
	}

	videoSearch(){
		this.videoParam.search = this.searchString;
		this.getvideolist();
	}
		
	paginationFeed(page){
        this.p = page;
        let start = (page - 1) * this.itemsPerPage;
        this.videoParam.page = page;
        this.videoApi.video(this.videoParam).subscribe(data=> {
            this.videoliting = data.data;
        });
        
	}
}


@Component({
	selector: 'app-reject-modal',
	templateUrl: '../proposal/reject-modal/reject-modal.component.html',
  })
  
  export class VideoDeleteModalComponent {
	public showLoader: boolean;
	constructor(
		public dialogRef: MatDialogRef<VideoDeleteModalComponent>,
		@Inject(MAT_DIALOG_DATA) public data:any
	) { }
   
	deleteVideo(){
		this.dialogRef.close({delStatus:'deleted'});
	}
	
	rejectproposal(){}

	deleteNews(){}

	deleteMember(){}
  
	onCloseClick(){
		this.dialogRef.close();
	}
  }

