import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideoListRouteModule} from './video-list.routes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from '../../../services/apis/auth.service';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { VideoService } from '../../../services/apis/video.service';
import { VideoListComponent,VideoDeleteModalComponent} from './video-list.component';
import { NgxPaginationModule } from 'ngx-pagination';
@NgModule({
  imports: [
    VideoListRouteModule,
    SharedModule,
    MatSortModule,
    NgxPaginationModule
  ],
  declarations: [
    VideoListComponent,
    VideoDeleteModalComponent
  ],
  entryComponents: [
    VideoListComponent,
    VideoDeleteModalComponent
  ],
  providers: [AuthService, UserMasterService, VideoService]
})
export class VideoListModule { }





