import { NgModule } from '@angular/core';
import { QuaterlyReportsRouteModule } from './quaterly-reports.routes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from 'src/app/services/apis/auth.service';
import { QuaterlyReportsComponent } from './quaterly-reports.component';
import { MatSortModule } from '@angular/material';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { QualityService } from 'src/app/services/apis/qualityreports.service';
import { UserEditService } from 'src/app/services/apis/useredit.service';
import { EditReportsComponent } from './edit-reports/edit-reports.component';
import { ProposalService } from 'src/app/services/apis/proposal.service';
import { NgxCurrencyModule } from "ngx-currency";
//import { RegistrationService } from 'src/app/services/apis/registration.service';
//import { UserEditService } from 'src/app/services/apis/useredit.service';

//UserEditService
@NgModule({
  imports: [
    QuaterlyReportsRouteModule,
    SharedModule,
    MatSortModule,
    NgxCurrencyModule
  ],
  
  declarations: [
    QuaterlyReportsComponent,
    EditReportsComponent
  ],
  entryComponents: [
    QuaterlyReportsComponent,
    EditReportsComponent
  ],
  providers: [AuthService, UserMasterService, QualityService, UserEditService, ProposalService]
})
export class QuaterlyReportsModule{

 
 }
