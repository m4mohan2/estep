import { AppConst } from 'src/app/app.constants';
import { Component, OnInit, Inject, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSelectChange } from '@angular/material';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { equalvalidator } from 'src/app/directives/validators/equal-validator';
import { QualityService } from 'src/app/services/apis/qualityreports.service';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-edit-reports',
  templateUrl: './edit-reports.component.html',
  styleUrls: ['./edit-reports.component.scss'],
  providers: []
})
export class EditReportsComponent implements OnInit {
  editReportform: FormGroup;
  showLoader: boolean;
  statevalidation: boolean;
  @Output() OnEditReport = new EventEmitter<any>(true);
  constructor(
    private datePipe: DatePipe,
    private formBulder: FormBuilder,
    private toast: ToastProvider,
    private dialogRef: MatDialogRef<EditReportsComponent>,
    private consoleProvider: ConsoleProvider,
    private UserMasterApi: UserMasterService,
    private quarterApi: QualityService,
    private translate: TranslateService,

    @Inject(MAT_DIALOG_DATA) public data: any

  ) {
    //console.log(JSON.stringify(this.data));
    this.statevalidation = false;
    
  }
  /*
  * function build form with exting data
  * @param
  * @memberof EditReportsComponent
  */
  ngOnInit() {
    //console.log(this.data)
    this.editReportform = new FormGroup({
      id: new FormControl(this.data.id, []),
      no_check: new FormControl(this.data.hasOwnProperty('no_check') ? this.data.no_check : '', [
        Validators.required,
      ]),
      infavourof: new FormControl(this.data.hasOwnProperty('infavourof') ? this.data.infavourof : '', [
        Validators.required,
      ]),
      date: new FormControl(this.data.hasOwnProperty('date') ? this.data.date : '', [
        Validators.required,
      ]),
      expense_amount: new FormControl(this.data.hasOwnProperty('expense_amount') ? this.data.expense_amount : '', [
        Validators.required,
      ]),
    });
  }
  
  /*
  * function use to api call  edit admin user
  * @param
  * @memberof EditReportsComponent
  */
  editReport(value) {
    var params={
        expense_amount:value.expense_amount,
        id:value.id, 
        date:this.datePipe.transform(value.date,"yyyy-MM-dd"), 
        no_check:value.no_check, 
        infavourof:value.infavourof
    }

    if (this.editReportform.valid) {
      this.showLoader = true;
      this.quarterApi.editExpenditure(params).subscribe((response: any) => {
        this.showLoader = false;
        // console.log(response);
        if (response.success) {
          this.OnEditReport.emit(response);
          this.dialogRef.close();
          this.toast.success(this.translate.instant('GLOBAL.UPDATE_SUCCESS'));
        }
      }, (errorData: any) => {
        this.showLoader = false;
      });
    }
  }
}
