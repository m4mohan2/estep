import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { QualityService } from 'src/app/services/apis/qualityreports.service';
import { ProposalService } from 'src/app/services/apis/proposal.service';
import { CommonService } from 'src/app/services/apis/common.service';
import { DatePipe } from '@angular/common';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { error } from 'util';
import { NgxUiLoaderService, SPINNER } from 'ngx-ui-loader';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { AppConst } from 'src/app/app.constants';
import { EditReportsComponent } from './edit-reports/edit-reports.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, PageEvent, MatSort } from '@angular/material';

@Component({
    selector: 'app-quaterly-reports',
    templateUrl: './quaterly-reports.component.html',
    styleUrls: ['./quaterly-reports.component.scss']
})
export class QuaterlyReportsComponent implements OnInit {
    spinnerType = SPINNER.rectangleBounce;
    apiBaseUrl: any;
	filevaidation: boolean;
	showLoader: boolean;
    DocMessage: string;
    newslistid: number;
    quaterlyReportform: FormGroup;
    f1submitted: boolean;
    p: number = 1;
    language: string;
    newsParam: any = {};
    msg;
    created_at = new Date(1990, 0, 1);
    successMsg: boolean = false;
    errorMsg: boolean = false;
    userid: number;
    filesToUpload: Array<File> = [];
    imageUrl: Array<any> = [];
    upFiles: any = [];
    allfiles: any = [];
    proposalid: any;
    public proposal: any;
    quater: any;
    loggedUserType: any;
    loggedUserRole: any;
    classificationData:any = [];
    evidenceData:any =[]
	fileUrl:string = environment.file_base_url
    loadingFile:boolean = false;
    //calculate the administrive and direct
    public ab_c1: number = 0;
    public ab_c2: number = 0;
    public ab_c3: number = 0;
    //public ab_c4: number = 0;
    public ab_total: number = 0;
    public db_c1: number = 0;
    public db_c2: number = 0;
    public db_c3: number = 0;
    //public db_c4: number = 0;
    public db_total: number = 0;
    public q1total: number = 0;
    public q2total: number = 0;
    public q3total: number = 0;
    //public q4total: number = 0;
    public alltotal: number = 0;
    public administrative_budget: number = 0;
    public direct_budget: number = 0;
    public gross: number = 0;
    public net: number = 0;
    public expenditureData:any = [];
    public directExpenditureData:any = [];
    public admExpenceData:any = [];
    public directExpenceData:any = [];
    public admSummaryData:any = [];
    public directSummaryData:any = [];
    public getUpdateBudgetData:any = [];

    administativeExpenditureForm;
    directExpenditureForm;
    evidenceForm;

    adminQat:any = []
    directQat:any = []

    reportEdit: boolean = true;

    public total_adm_tight_budget: number = 0;
    public total_direct_tight_budget: number = 0;

    public total_adm_expenses_budget: number = 0;
    public total_direct_expenses_budget: number = 0;

    public total_adm_remain_amt: number = 0;
    public total_direct_remain_amt: number = 0;

    public adm_remain_color: string = '#2c2c2c';
    public direct_remain_color: string = '#2c2c2c';

    public total_adm_reprog_budget1: number = 0;
    public total_adm_reprog_budget2: number = 0;
    public total_adm_reprog_budget3: number = 0;
    public total_adm_reprog_budget4: number = 0;
    public total_adm_reprog_budget5: number = 0;

    public total_direct_reprog_budget1: number = 0;
    public total_direct_reprog_budget2: number = 0;
    public total_direct_reprog_budget3: number = 0;
    public total_direct_reprog_budget4: number = 0;
    public total_direct_reprog_budget5: number = 0;

    constructor(
        private toast: ToastProvider,
        private quarterApi: QualityService,
        //private ExpensesApi: QualityService,
        private proposalApi: ProposalService,
        private common: CommonService,
        public router: Router,
		private route: ActivatedRoute,
        private datePipe: DatePipe,
        private userStorage: UserStorageProvider,
        private ngxService: NgxUiLoaderService,
        private sanitizer: DomSanitizer,
        private translate: TranslateService,
        public dialog: MatDialog,
    ) {
        this.newsformbuild();
        this.filevaidation = false;
        this.DocMessage = '';
		this.newslistid = 0;
        this.showLoader = false;
        this.loggedUserType = this.userStorage.get().user.user_type;
        this.loggedUserRole = this.userStorage.get().user.role.roleKey;
        this.apiBaseUrl = AppConst.API_BASE_URL;
        //console.log(this.loggedUserRole)
      
    }
    ngOnInit() {
        //console.log("quater",this.quater)
        this.newsParam.page = this.p;

        this.common.currLang.subscribe(
            res => {
                this.language = res;
            }
        )
        this.route.params.subscribe(params => {
            //console.log("gg",params)
            if (params) {
                //console.log(params);
                if (params['id']) {
                    //console.log(params['id']);
                    this.proposalid = +params['id'];
                    if (this.proposalid > 0) {
                        //this.getuserdetaiils();
                        this.getProposal();
        
                    }
				}
                if(params['param']){
                    this.quater = +params['param'];
                }
            }
        });
        this.getClassification();
        this.getEvidenceList(); 
        
        this.getExpenditureList();
        this.getDirectExpenditureList();
        this.getExpenceList();

    }

    getExpenceList(){
        this.getAdmExpenceList();
        this.getDirectExpenceList();
        this.getAdmSummaryList();
        this.getDirectSummaryList();
    }


    getuserdetaiils = () => {
     
        this.quarterApi.qualityreports(this.proposalid).subscribe(
            res => {
                //console.log('getuserdetaiils=>', res);
                this.quaterlyReportform.patchValue({
                    id: this.proposalid,
                    entity_name: res.data[0].entity_name,
                    entity_code: res.data[0].entity_code,
                    created_at: this.datePipe.transform(res.data[0].created_at,"MM-dd-yyyy"),
					proposal_unique_id: res.data[0].proposal_unique_id,
					//administrative_budget: res.data[0].administrative_budget,
					//direct_budget: res.data[0].direct_budget,
					ab_c1: res.data[0].ab_c1,
					ab_c2: res.data[0].ab_c2,
					ab_c3: res.data[0].ab_c3,
					db_c1: res.data[0].db_c1,
					db_c2: res.data[0].db_c2,
					db_c3: res.data[0].db_c3,
					gross: res.data[0].gross,
					q1total: res.data[0].q1total,
					q2total: res.data[0].q2total,
					q3total: res.data[0].q3total,
					alltotal: res.data[0].alltotal,
					net: res.data[0].net,
					ab_total: res.data[0].ab_total,
					db_total: res.data[0].db_total,
                    //projectNameEn: res.data[0].projectNameEn,
                    //endDate: res.data[0].endDate,
				});
				
				if(res.data[0].db_c2 === null && res.data[0].db_c3 === null) {
					this.quaterlyReportform.patchValue({
						db_c2:0,
						db_c3:0
					});
				}

                //console.log("44444444444",this.quater)

                if(this.quater == 1) {
                    this.quaterlyReportform.patchValue({
                        reportstatus:res.data[0].reportstatus1
                    });

                    this.reportEdit = (res.data[0].reportstatus1 == 1 || res.data[0].reportstatus1 == 3 ) && this.loggedUserRole==='companies' ? false : true;
                }
                if(this.quater == 2) {
                    this.quaterlyReportform.patchValue({
                        reportstatus:res.data[0].reportstatus2
                    });

                    this.reportEdit = (res.data[0].reportstatus2 == 1 || res.data[0].reportstatus2 == 3 ) && this.loggedUserRole==='companies' ? false : true;
                }
                if(this.quater == 3) {
                    this.quaterlyReportform.patchValue({
                        reportstatus:res.data[0].reportstatus3
                    });

                    this.reportEdit = (res.data[0].reportstatus3 == 1 || res.data[0].reportstatus3 == 3 ) && this.loggedUserRole==='companies' ? false : true;
                }
            }
        )
    }


    getProposal(){
        
        this.proposalApi.viewProposal().subscribe((res: any) => {
           
            if (res.success) {
                this.proposal = res.data;
                if(res.data.budget_breakdown.length > 0){
                  
                    this.total_adm_remain_amt = 0;
                    this.total_direct_remain_amt = 0;

                    this.adm_remain_color = '';
                    this.direct_remain_color = '';

                    this.total_adm_reprog_budget1 = 0;
                    this.total_adm_reprog_budget2 = 0;
                    this.total_adm_reprog_budget3 = 0;
                    this.total_adm_reprog_budget4 = 0;
                    this.total_adm_reprog_budget5 = 0;

                    this.total_direct_reprog_budget1 = 0;
                    this.total_direct_reprog_budget2 = 0;
                    this.total_direct_reprog_budget3 = 0;
                    this.total_direct_reprog_budget4 = 0;
                    this.total_direct_reprog_budget5 = 0; 

                    for(let item of res.data.budget_breakdown){
                        if(item.budget_type == 'Administrative'){
                          this.total_adm_tight_budget += item.tight_budget;
                          this.total_adm_remain_amt += item.remain_amount;

                            const itemWiseAdminExpenseList = res.data.reported_expenses.filter(expenseitems => expenseitems.bb_id == item.id);
                            
                            for(let i=0; i<itemWiseAdminExpenseList.length; i++){
                                this.total_adm_expenses_budget+=itemWiseAdminExpenseList[i].expense_amount;
                            }

                          this.total_adm_reprog_budget1 += item.reprog_budget1;
                          this.total_adm_reprog_budget2 += item.reprog_budget2;
                          this.total_adm_reprog_budget3 += item.reprog_budget3;
                          this.total_adm_reprog_budget4 += item.reprog_budget4;
                          this.total_adm_reprog_budget5 += item.reprog_budget5;
                        }

                        if(item.budget_type == 'Direct'){
                          this.total_direct_tight_budget += item.tight_budget;
                          this.total_direct_remain_amt += item.remain_amount;

                            const itemWiseDirectExpenseList = res.data.reported_expenses.filter(expenseitems => expenseitems.bb_id == item.id);
                            
                            for(let i=0; i<itemWiseDirectExpenseList.length; i++){
                                this.total_direct_expenses_budget+=itemWiseDirectExpenseList[i].expense_amount;
                            }

                          this.total_direct_reprog_budget1 += item.reprog_budget1;
                          this.total_direct_reprog_budget2 += item.reprog_budget2;
                          this.total_direct_reprog_budget3 += item.reprog_budget3;
                          this.total_direct_reprog_budget4 += item.reprog_budget4;
                          this.total_direct_reprog_budget5 += item.reprog_budget5;
                        }
                    }

                    this.total_adm_remain_amt = this.total_adm_tight_budget - this.total_adm_expenses_budget;
                    this.total_direct_remain_amt = this.total_direct_tight_budget - this.total_direct_expenses_budget;

                    
                   
                    if( this.total_adm_remain_amt < 0) {
                        this.adm_remain_color = 'red';
                    }

                    if( this.total_direct_remain_amt < 0) {
                        this.direct_remain_color = 'red';
                    }
                
                    ///## Reprograming Budget Breakdown

                    // 1) If the approved budget "Grant Final" is equal to the "Grant Requested"
                    // and the budget was not adjusted, the amount of the summary of expenses in the quarterly reports is the sum of the field "Budget_Amount" of all the items of the Budget_breakdowns for Administrative and Direct.


                    // 2) If the approved budget "Grant Final" is equal to the "Grant Requested"
                    // and the budget was adjusted, the amount of the summary of expenses in the quarterly reports is the sum of the field "Tight_budget" of all the items of the Budget_breakdowns for Administrative and Direct.

                    // 3) If the approved budget "Grant Final" is different "Grant Requested"
                    // and the budget was adjusted, the amount of the summary of expenses in the quarterly reports is the sum of the field "Tight_budget" of all the items of the Budget_breakdowns for Administrative and Direct.

                    if ( this.total_adm_tight_budget > 0){
                        this.quaterlyReportform.patchValue({
                            administrative_budget: this.total_adm_tight_budget,
                        });
                    }
                    if ( this.total_direct_tight_budget > 0){
                        this.quaterlyReportform.patchValue({
                            direct_budget: this.total_direct_tight_budget,
                        });
                    }

                    // 4) If the proposal has a reprogramming budget, the amount of the summary of expenses in the quarterly reports is the sum of the field "reprog_budget#" of all the items of the Budget_breakdowns for Administrative and Direct.
                    switch ( res.data.reprogramCount ) {
                        case 1:
                            if ( this.total_adm_reprog_budget1 > 0){
                                this.quaterlyReportform.patchValue({
                                    administrative_budget: this.total_adm_reprog_budget1,
                                });
                            }
                            if ( this.total_direct_reprog_budget1 > 0){
                                this.quaterlyReportform.patchValue({
                                    direct_budget: this.total_direct_reprog_budget1,
                                });
                            }
                          break;
                        case 2:
                          if ( this.total_adm_reprog_budget2 > 0){
                                this.quaterlyReportform.patchValue({
                                    administrative_budget: this.total_adm_reprog_budget2,
                                });
                            }
                            if ( this.total_direct_reprog_budget2 > 0){
                                this.quaterlyReportform.patchValue({
                                    direct_budget: this.total_direct_reprog_budget2,
                                });
                            }
                          break;
                        case 3:
                          if ( this.total_adm_reprog_budget3 > 0){
                                this.quaterlyReportform.patchValue({
                                    administrative_budget: this.total_adm_reprog_budget3,
                                });
                            }
                            if ( this.total_direct_reprog_budget3 > 0){
                                this.quaterlyReportform.patchValue({
                                    direct_budget: this.total_direct_reprog_budget3,
                                });
                            }
                          break;
                        case 4:
                          if ( this.total_adm_reprog_budget4 > 0){
                                this.quaterlyReportform.patchValue({
                                    administrative_budget: this.total_adm_reprog_budget4,
                                });
                            }
                            if ( this.total_direct_reprog_budget4 > 0){
                                this.quaterlyReportform.patchValue({
                                    direct_budget: this.total_direct_reprog_budget4,
                                });
                            }
                        case 5:
                          if ( this.total_adm_reprog_budget5 > 0){
                                this.quaterlyReportform.patchValue({
                                    administrative_budget: this.total_adm_reprog_budget5,
                                });
                            }
                            if ( this.total_direct_reprog_budget5 > 0){
                                this.quaterlyReportform.patchValue({
                                    direct_budget: this.total_direct_reprog_budget5,
                                });
                            }
                            
                    }


                    this.quarterApi.qualityreports(this.proposalid).subscribe(
                        res => {
                            //console.log('getuserdetaiils=>', res);
                            this.quaterlyReportform.patchValue({
                                id: this.proposalid,
                                entity_name: res.data[0].entity_name,
                                entity_code: res.data[0].entity_code,
                                created_at: this.datePipe.transform(res.data[0].created_at,"MM-dd-yyyy"),
                                proposal_unique_id: res.data[0].proposal_unique_id,
                                //administrative_budget: res.data[0].administrative_budget,
                                //direct_budget: res.data[0].direct_budget,
                                ab_c1: res.data[0].ab_c1,
                                ab_c2: res.data[0].ab_c2,
                                ab_c3: res.data[0].ab_c3,
                                db_c1: res.data[0].db_c1,
                                db_c2: res.data[0].db_c2,
                                db_c3: res.data[0].db_c3,
                                gross: res.data[0].gross,
                                q1total: res.data[0].q1total,
                                q2total: res.data[0].q2total,
                                q3total: res.data[0].q3total,
                                alltotal: res.data[0].alltotal,
                                net: res.data[0].net,
                                ab_total: res.data[0].ab_total,
                                db_total: res.data[0].db_total,
                                //projectNameEn: res.data[0].projectNameEn,
                                //endDate: res.data[0].endDate,
                            });
                            
                            if(res.data[0].db_c2 === null && res.data[0].db_c3 === null) {
                                this.quaterlyReportform.patchValue({
                                    db_c2:0,
                                    db_c3:0
                                });
                            }
            
                            //console.log("44444444444",this.quater)
            
                            if(this.quater == 1) {
                                this.quaterlyReportform.patchValue({
                                    reportstatus:res.data[0].reportstatus1
                                });
            
                                this.reportEdit = (res.data[0].reportstatus1 == 1 || res.data[0].reportstatus1 == 3 ) && this.loggedUserRole==='companies' ? false : true;
                            }
                            if(this.quater == 2) {
                                this.quaterlyReportform.patchValue({
                                    reportstatus:res.data[0].reportstatus2
                                });
            
                                this.reportEdit = (res.data[0].reportstatus2 == 1 || res.data[0].reportstatus2 == 3 ) && this.loggedUserRole==='companies' ? false : true;
                            }
                            if(this.quater == 3) {
                                this.quaterlyReportform.patchValue({
                                    reportstatus:res.data[0].reportstatus3
                                });
            
                                this.reportEdit = (res.data[0].reportstatus3 == 1 || res.data[0].reportstatus3 == 3 ) && this.loggedUserRole==='companies' ? false : true;
                            }
                        }
                    )

                    

                }
            }

        }, (err) => {});
    }


    ngAfterViewInit(){
      
        if(this.quater < 4){
            this.administativeExpenditureForm.controls["quarter"].setValue('Q' + this.quater);
            this.directExpenditureForm.controls["quarter"].setValue('Q' + this.quater);
            this.evidenceForm.controls["quarter"].setValue('Q' + this.quater);
        }
    }


    getExpenditureList(){
       
        this.ngxService.start()
        this.quarterApi.getExpenditureList(this.proposalid).subscribe(
            res=>{
                //console.log(res)
                this.ngxService.stop()
                this.expenditureData = res['data']
            }
        )
    }
    getDirectExpenditureList(){
      
        this.ngxService.start()
        this.quarterApi.getDirectExpenditureList(this.proposalid).subscribe(
            res=>{
                //console.log(res)
                this.ngxService.stop()
                this.directExpenditureData = res['data']
            }
        )
    }

    getAdmExpenceList(){
       
        this.ngxService.start()
        this.quarterApi.getAdmExpenceList(this.proposalid, this.quater).subscribe(
            res=>{
                //console.log(res)
                this.ngxService.stop()
                this.admExpenceData = res['data'];

                this.adminQat = [];
                for (let item of this.admExpenceData) {
                    this.adminQat.push(item.quarter);
                }
                this.adminQat = [...new Set(this.adminQat)]
                //console.log("adminQat", this.adminQat);
            }
        )
    }

    getDirectExpenceList(){
       
        this.ngxService.start()
        this.quarterApi.getDirectExpenceList(this.proposalid, this.quater).subscribe(
            res=>{
                //console.log(res)
                this.ngxService.stop()
                this.directExpenceData = res['data'];

                this.directQat = [];
                for (let item of this.directExpenceData) {
                    this.directQat.push(item.quarter);
                }
                this.directQat = [...new Set(this.directQat)]
                //console.log("directQat", this.directQat);

            }
        )
    }

    getAdmSummaryList(){
      
        this.quarterApi.getSummaryList(this.proposalid, this.quater, 'Administrative').subscribe(
            res=>{
                //console.log(res)
                this.admSummaryData = res['data'];

                this.admSummaryData.forEach((currentValue, index) => {
                    if (currentValue.reprog_budget5) {
                        currentValue.tight_budget=currentValue.reprog_budget5;
                    } else if (currentValue.reprog_budget4) {
                        currentValue.tight_budget=currentValue.reprog_budget4;
                    }else if (currentValue.reprog_budget3) {
                        currentValue.tight_budget=currentValue.reprog_budget3;
                    }else if (currentValue.reprog_budget2) {
                        currentValue.tight_budget=currentValue.reprog_budget2;
                    }else if (currentValue.reprog_budget1) {
                        currentValue.tight_budget=currentValue.reprog_budget1;
                    } else {
                        currentValue.tight_budget=currentValue.tight_budget;
                    }

                    currentValue.remain_amount= parseFloat(currentValue.remain_amount.toFixed(2));
                   
                }); 
            }
        )
    }

    getDirectSummaryList(){
     
        this.quarterApi.getSummaryList(this.proposalid, this.quater , 'Direct').subscribe(
            res=>{
                //console.log(res)
                this.directSummaryData = res['data'];

                this.directSummaryData.forEach((currentDValue, index) => {
                    if (currentDValue.reprog_budget5) {
                        currentDValue.tight_budget=currentDValue.reprog_budget5;
                    } else if (currentDValue.reprog_budget4) {
                        currentDValue.tight_budget=currentDValue.reprog_budget4;
                    }else if (currentDValue.reprog_budget3) {
                        currentDValue.tight_budget=currentDValue.reprog_budget3;
                    }else if (currentDValue.reprog_budget2) {
                        currentDValue.tight_budget=currentDValue.reprog_budget2;
                    }else if (currentDValue.reprog_budget1) {
                        currentDValue.tight_budget=currentDValue.reprog_budget1;
                    } else {
                        currentDValue.tight_budget=currentDValue.tight_budget;
                    }
                    currentDValue.remain_amount= parseFloat(currentDValue.remain_amount.toFixed(2));
                }); 
            }
        )
    }


    calculate(event) {
        this.ab_total = (this.ab_c1) + (this.ab_c2) + (this.ab_c3);
        this.db_total = (this.db_c1) + (this.db_c2) + (this.db_c3);
        this.q1total = (this.ab_c1) + (this.db_c1);
        this.q2total = (this.ab_c2) + (this.db_c2);
        this.q3total = (this.ab_c3) + (this.db_c3);
        //this.q4total = (this.ab_c4) + (this.db_c4);
        this.alltotal = (this.ab_total) + (this.db_total);
        this.gross = (this.administrative_budget) + (this.direct_budget);
        this.net = (this.gross) - (this.alltotal);
    }
    
    newsformbuild() {
        this.administativeExpenditureForm = new FormGroup({
            quarter: new FormControl('', [Validators.required]),
            expenditure: new FormControl('', [Validators.required]),
            dateofincorporationAdmintrator: new FormControl('', [Validators.required]),
            chequeNo: new FormControl('', [Validators.required]),
            inFavourOf: new FormControl('', [Validators.required]),
            amount: new FormControl('', [Validators.required]),
        })

        this.directExpenditureForm = new FormGroup({
            quarter: new FormControl('', [Validators.required]),
            expenditure: new FormControl('', [Validators.required]),
            dateofincorporationDirect: new FormControl('', [Validators.required]),
            chequeNo: new FormControl('', [Validators.required]),
            inFavourOf: new FormControl('', [Validators.required]),
            amount: new FormControl('', [Validators.required]),
        })

        this.evidenceForm = new FormGroup({
            quarter: new FormControl('', [Validators.required]),
            classification: new FormControl('', [Validators.required]),
        })
        this.quaterlyReportform = new FormGroup({
            entity_name: new FormControl(''),
            entity_code: new FormControl(''),
            created_at: new FormControl(''),
            proposal_unique_id: new FormControl(''),
            user_name: new FormControl(''),

            administrative_budget: new FormControl(''),
            ab_c1: new FormControl(''),
            ab_c2: new FormControl(''),
            ab_c3: new FormControl(''),
            direct_budget: new FormControl('', [Validators.required]),
            db_c1: new FormControl(''),
            db_c2: new FormControl(''),
            db_c3: new FormControl(''),
			ab_total: new FormControl(''),
			db_total: new FormControl(''),
            q1total: new FormControl(''),
            q2total: new FormControl(''),
            q3total: new FormControl(''),
            alltotal: new FormControl(''),
            gross: new FormControl(''),
			net: new FormControl(''),
            reportstatus: new FormControl(''),

			//name: new FormControl('', [Validators.required]),
			//currentdate: new FormControl('', [Validators.required]),
			
        });
    }
    get f1() { return this.quaterlyReportform.controls; };

    addReport(value) {
        this.f1submitted = true;

        if (this.quaterlyReportform.invalid) { this.toast.error(this.translate.instant('MESSAGE.REQUIRED_MESSAGE')); return; }
            const formData = new FormData();
            var qualityValue = {
                ab_c1: value.ab_c1,
                ab_c2: value.ab_c2,
                ab_c3: value.ab_c3,
                db_c1: value.db_c1,
                db_c2: value.db_c2,
                db_c3: value.db_c3,
                proposal_id:this.proposalid,
                administrative_budget:value.administrative_budget,
    			direct_budget:value.direct_budget,

                reportstatus:value.reportstatus,
                quater:this.quater
    			
            }
            //console.log(qualityValue)

        this.quarterApi.expensesummary(qualityValue).subscribe
            (
            (res: any) => {
                this.getuserdetaiils();
				this.msg = res['message'];
				this.showLoader = false;
                this.router.navigate(['post-auth/proposal-list']);
                this.toast.success(this.msg)
                this.quaterlyReportform.reset();
                setTimeout(() => {
                    this.errorMsg = false;

                }, 3000);
            },
            (err) => {
                this.f1submitted = false;
                this.msg = err['message'];
                this.toast.error(this.msg)
            }
        );

    }
    
    onSubmit(value){
           
        const itemWiseAdminExpenseList = this.proposal.budget_breakdown.filter(expenseitems => expenseitems.id == value.expenditure);

        if(itemWiseAdminExpenseList[0].remain_amount>0){
            itemWiseAdminExpenseList.remain_amount=itemWiseAdminExpenseList[0].remain_amount;
        }else {

            if(this.proposal.application_status ==5 && this.proposal.reprogramStatus ==1 && this.proposal.reprogramCount ==0){
                       
                itemWiseAdminExpenseList.remain_amount=itemWiseAdminExpenseList[0].tight_budget;
               
            }else if(this.proposal.application_status ==5 && this.proposal.reprogramStatus ==1 && this.proposal.reprogramCount ==1){
            
                itemWiseAdminExpenseList.remain_amount=itemWiseAdminExpenseList[0].reprog_budget1;
            
            }else if(this.proposal.application_status ==5 && this.proposal.reprogramStatus ==1 && this.proposal.reprogramCount ==2){
            
                itemWiseAdminExpenseList.remain_amount=itemWiseAdminExpenseList[0].reprog_budget2;
            
            }else if(this.proposal.application_status ==5 && this.proposal.reprogramStatus ==1 && this.proposal.reprogramCount ==3){
            
                itemWiseAdminExpenseList.remain_amount=itemWiseAdminExpenseList[0].reprog_budget3;
            
            }else if(this.proposal.application_status ==5 && this.proposal.reprogramStatus ==1 && this.proposal.reprogramCount ==4){
            
                itemWiseAdminExpenseList.remain_amount=itemWiseAdminExpenseList[0].reprog_budget4;
            
            }else if(this.proposal.application_status ==5 && this.proposal.reprogramStatus ==1 && this.proposal.reprogramCount ==5){
                
                itemWiseAdminExpenseList.remain_amount=itemWiseAdminExpenseList[0].reprog_budget5;
            
            }
        }
       
        if(value.amount>itemWiseAdminExpenseList.remain_amount) { this.toast.error(this.translate.instant('MESSAGE.EXPENSE_AMOUT_EXCEED')); return; }


        this.total_adm_tight_budget=0;
        this.total_direct_tight_budget=0;
        this.total_direct_expenses_budget = 0;
        this.total_adm_expenses_budget = 0;
        
        var addParam={
            proposal_id:this.proposalid, 
            bb_id:value.expenditure, 
            quarter:value.quarter, 
            date:this.datePipe.transform(value.dateofincorporationAdmintrator,"yyyy-MM-dd"), 
            expense_amount:value.amount, 
            no_check:value.chequeNo, 
            infavourof:value.inFavourOf
        }
        if (this.administativeExpenditureForm.invalid) { this.toast.error(this.translate.instant('MESSAGE.REQUIRED_MESSAGE')); return; }

        this.ngxService.start()
        this.quarterApi.saveAdministrativeExpenditure(addParam).subscribe(
            res=>{
                if(res.success){
                    this.ngxService.stop()
                   
                    this.administativeExpenditureForm.reset()
                    this.msg = res['message'];
                    this.toast.success(this.msg) 

                    if(this.quater < 4){
                        this.administativeExpenditureForm.controls["quarter"].setValue('Q' + this.quater);
                        this.directExpenditureForm.controls["quarter"].setValue('Q' + this.quater);
                    }
                    //this.getuserdetaiils();
                    this.getProposal();
                    this.getAdmExpenceList();
                    this.getAdmSummaryList();
                   
                }
            },
            error=>{
                this.ngxService.stop()
                this.msg = error['message'];
                this.toast.error(this.msg) 
            }
        )
    }

    onSubmitDirect(value){

        const itemWiseAdminExpenseList = this.proposal.budget_breakdown.filter(expenseitems => expenseitems.id == value.expenditure);

        if(itemWiseAdminExpenseList[0].remain_amount>0){
            itemWiseAdminExpenseList.remain_amount=itemWiseAdminExpenseList[0].remain_amount;
        }else {

            if(this.proposal.application_status ==5 && this.proposal.reprogramStatus ==1 && this.proposal.reprogramCount ==0){
                       
                itemWiseAdminExpenseList.remain_amount=itemWiseAdminExpenseList[0].tight_budget;
               
            }else if(this.proposal.application_status ==5 && this.proposal.reprogramStatus ==1 && this.proposal.reprogramCount ==1){
            
                itemWiseAdminExpenseList.remain_amount=itemWiseAdminExpenseList[0].reprog_budget1;
            
            }else if(this.proposal.application_status ==5 && this.proposal.reprogramStatus ==1 && this.proposal.reprogramCount ==2){
            
                itemWiseAdminExpenseList.remain_amount=itemWiseAdminExpenseList[0].reprog_budget2;
            
            }else if(this.proposal.application_status ==5 && this.proposal.reprogramStatus ==1 && this.proposal.reprogramCount ==3){
            
                itemWiseAdminExpenseList.remain_amount=itemWiseAdminExpenseList[0].reprog_budget3;
            
            }else if(this.proposal.application_status ==5 && this.proposal.reprogramStatus ==1 && this.proposal.reprogramCount ==4){
            
                itemWiseAdminExpenseList.remain_amount=itemWiseAdminExpenseList[0].reprog_budget4;
            
            }else if(this.proposal.application_status ==5 && this.proposal.reprogramStatus ==1 && this.proposal.reprogramCount ==5){
                
                itemWiseAdminExpenseList.remain_amount=itemWiseAdminExpenseList[0].reprog_budget5;
            
            }
        }
       
        if(value.amount>itemWiseAdminExpenseList.remain_amount) { this.toast.error(this.translate.instant('MESSAGE.EXPENSE_AMOUT_EXCEED')); return; }
      
        this.total_adm_tight_budget=0;
        this.total_direct_tight_budget=0;
        this.total_direct_expenses_budget = 0;
        this.total_adm_expenses_budget = 0;

        var addParam={
            proposal_id:this.proposalid, 
            bb_id:value.expenditure, 
            quarter:value.quarter, 
            date:this.datePipe.transform(value.dateofincorporationDirect,"yyyy-MM-dd"), 
            expense_amount:value.amount, 
            no_check:value.chequeNo, 
            infavourof:value.inFavourOf
        }
        if (this.directExpenditureForm.invalid) { this.toast.error(this.translate.instant('MESSAGE.REQUIRED_MESSAGE')); return; }
        
        
        this.ngxService.start()
        this.quarterApi.saveAdministrativeExpenditure(addParam).subscribe(
            res=>{
                if(res.success){
                    this.ngxService.stop()
                    this.directExpenditureForm.reset()
                    this.msg = res['message'];
                    this.toast.success(this.msg) 

                    if(this.quater < 4){
                        this.administativeExpenditureForm.controls["quarter"].setValue('Q' + this.quater);
                        this.directExpenditureForm.controls["quarter"].setValue('Q' + this.quater);
                    }
                    //this.getuserdetaiils();
                    this.getProposal();
                    this.getDirectExpenceList();
                    this.getDirectSummaryList();
                }
            },
            error=>{
                this.ngxService.stop()
                this.msg = error['message'];
                this.toast.error(this.msg) 
            }
        )
    }

    onClickDeleteAdminExpance(event){
        this.total_adm_tight_budget=0;
        this.total_direct_tight_budget=0;
        this.total_direct_expenses_budget = 0;
        this.total_adm_expenses_budget = 0;
       
        this.quarterApi.deleteAdministratorDirectExpance(event).subscribe(
            res=>{
                if(res.success){
                    this.getAdmExpenceList();
                    this.getAdmSummaryList();
                    this.msg = res['message'];
                    this.toast.success(this.msg);

                    //this.getuserdetaiils();
                    this.getProposal();
                }
            },
            error=>{
                this.msg = error['message'];
                this.toast.error(this.msg) 
            }
        )
    }


    onClickDeleteDirectExpance(event){
        this.total_adm_tight_budget=0;
        this.total_direct_tight_budget=0;
        this.total_direct_expenses_budget = 0;
        this.total_adm_expenses_budget = 0;
       

        this.quarterApi.deleteAdministratorDirectExpance(event).subscribe(
            res=>{
                if(res.success){
                    this.getDirectExpenceList();
                    this.getDirectSummaryList();
                    this.msg = res['message'];
                    this.toast.success(this.msg);
                    //this.getuserdetaiils();
                    this.getProposal();
                }
            },
            error=>{
                this.msg = error['message'];
                this.toast.error(this.msg) 
            }
        )
    }

    onClickEditAdminExpance(object){
        const dialogRef = this.dialog.open(EditReportsComponent, {
          panelClass: 'dialog-sm',
          disableClose: true,
          data: object
        });
        dialogRef.componentInstance.OnEditReport.subscribe((data: any) => {
          
        this.total_adm_tight_budget=0;
        this.total_direct_tight_budget=0;
        this.total_direct_expenses_budget = 0;
        this.total_adm_expenses_budget = 0;
        
          this.getExpenceList();
          this.getProposal();
        });
    }

    getClassification(){
        this.ngxService.start()
        this.quarterApi.getClassification().subscribe(
            res=>{
                this.ngxService.stop()
                this.classificationData = res['data']
            }
        )
    }
    onChangeClassification(event){
        console.log(event)
    }
    fileChangeEvent(fileInput: any) {
        this.filesToUpload = < Array < File >> fileInput.target.files;
        let files = fileInput.target.files;
        if (files) {
            for (let i = 0; i < files.length; i++) {
                const fileObj = {
                    name: '',
                    type: '',
                    url: ''
                }
                this.allfiles.push(files[i]);
                fileObj.name = files[i].name;
                fileObj.type = files[i].type;
        
                const reader = new FileReader();
                reader.onload = (filedata) => {
                    fileObj.url = reader.result + '';
                    this.upFiles.push(fileObj);
                }
                reader.readAsDataURL(files[i]);
            }
        }
    }
    removeFile(file: any) {
        const index = this.upFiles.indexOf(file);
        this.upFiles.splice(index, 1);
        this.allfiles.splice(index, 1);
    }
    onSubmitEvidence(value){
        this.loadingFile = true;
        if (this.evidenceForm.invalid) { 
            this.loadingFile = false;
            this.toast.error(this.translate.instant('PROPOSAL.SELECT_EVIDENCE_ERR')); 
            return; 
        }
        const formData: any = new FormData();

        if(this.allfiles.length > 0){
            const files: Array<File> = this.allfiles;
            
            if(files.length > 0){
                for(let i =0; i < files.length; i++){
                    formData.append("programmatic_report[]", files[i], files[i]['name']);
                }
            }
        }
        else{
          this.loadingFile = false;
          this.toast.error(this.translate.instant('MESSAGE.FILE_ATTACH')); return; 
        }

        formData.append('proposal_id', this.proposalid);
        formData.append('evidence_id', this.evidenceForm.value.classification);
        formData.append('quarter', this.evidenceForm.value.quarter);

        this.ngxService.start()
        this.quarterApi.saveEvidence(formData).subscribe(
            res=>{
                if(res.success){
                    this.evidenceForm.controls["classification"].setValue('');
                    this.upFiles = [];
                    this.allfiles = [];
                    this.loadingFile = false;
                    this.ngxService.stop()
                    this.getEvidenceList()
                    this.msg = res['message'];
                    this.toast.success(this.msg) 
                }
            },
            error=>{
                this.loadingFile = false;
                this.ngxService.stop()
                this.msg = error['message'];
                this.toast.error(this.msg) 
            }
        )
    }

    getEvidenceList(){
        this.ngxService.start()
        this.quarterApi.getEvidenceList(this.proposalid, this.quater).subscribe(
            res=>{
                this.ngxService.stop()
                this.evidenceData = res['data']
            }
        )
    }
    onClickDeleteEvidence(event){
        this.quarterApi.deleteEvidence(event).subscribe(
            res=>{
                if(res.success){
                    this.getEvidenceList()
                    this.msg = res['message'];
                    this.toast.success(this.msg) 
                }
            },
            error=>{
                this.msg = error['message'];
                this.toast.error(this.msg) 
            }
        )
    }
    
    onActivate(event) {
        window.scroll(0, 0);
    }

    goToReport(param: any) {
     
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate(['post-auth/quaterly-reports/' + localStorage.getItem('proposal_id') + '/' + param]);
    }


    getUpdateBudget(){
        
        this.ngxService.start()
        this.quarterApi.getUpdateBudget(this.proposalid).subscribe(
            res=>{
                //console.log(res)
                this.ngxService.stop()
                this.getUpdateBudgetData = res['data']

                this.quaterlyReportform.patchValue({
					administrative_budget: res.data[0].administrative_budget,
					direct_budget: res.data[0].direct_budget,
				});
            }
        )
    }

}
