import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuaterlyReportsComponent } from './quaterly-reports.component';

describe('QuaterlyReportsComponent', () => {
  let component: QuaterlyReportsComponent;
  let fixture: ComponentFixture<QuaterlyReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuaterlyReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuaterlyReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
