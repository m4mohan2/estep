import { NgModule } from '@angular/core';
// import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PostAuthComponent } from './post-auth.component';
import { PostAuthRouteModule } from './post-auth.routes.module';
import { ComponentsModule } from '../../components/components.module';
import { AppMaterialModule } from '../../app.material.module';
import { AuthService } from '../../services/apis/auth.service';
import { SharedModule } from '../../shared/shared.module';
import { CryptoProvider } from '../../services/crypto/crypto.service';
import { NgxMaskModule } from 'ngx-mask';
import { FormsModule } from '@angular/forms';
import { OwlModule } from 'ngx-owl-carousel';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { NgxPaginationModule } from 'ngx-pagination';
import { ChartsModule, ThemeService  } from 'ng2-charts';
import { OwlDateTimeModule, OwlNativeDateTimeModule,OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';

import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { NavigationComponent } from './navigation/navigation.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { ChangepasswordComponent } from './change-password/change-password.component';
import { ApproveModalComponent } from './proposal/approve-modal/approve-modal.component';
import { RejectModalComponent } from './proposal/reject-modal/reject-modal.component';
import { MoreInfoComponent } from './proposal/more-info/more-info.component';
import { EntityInfoComponent } from './proposal/entity-info/entity-info.component';
import { AppHistoryComponent } from './proposal/app-history/app-history.component';
import { PopupModalsComponent } from './proposal/popup-modals/popup-modals.component';
import { AssignModalsComponent } from './proposal-list/assign-modals/assign-modals.component';
import { AttachPopupComponent } from './proposal/attach-popup/attach-popup.component';
import { ReprogramModalComponent } from './proposal/reprogram-modal/reprogram-modal.component';
import { RequestModalComponent } from './proposal/request-modal/request-modal.component';
import { RejectGrantModalComponent } from './proposal/reject-grant-modal/reject-grant-modal.component';
import { PaidModalComponent } from './proposal/paid-modal/paid-modal.component';
import { WatingModalComponent } from './proposal/wating-modal/wating-modal.component';
import { CmsModalComponent } from './cms-modal/cms-modal.component';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { AppConst } from 'src/app/app.constants';

import { SidenavService } from 'src/app/services/side-nav.service';

export function HttpLoaderFactory(HttpClient: HttpClient) {
  return new TranslateHttpLoader(HttpClient,AppConst.PROJECT_FOLDER+'assets/i18n/', '.json');
}


@NgModule({
  imports: [
    PostAuthRouteModule,
    ComponentsModule,
    AppMaterialModule,
    SharedModule,
    NgxUiLoaderModule,
    NgxMaskModule.forRoot(),
    FormsModule,
    OwlModule,
    ChartsModule,
    NgxPaginationModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      
        
      }
    }),
  ],
  declarations: [
    PostAuthComponent,
    DashboardComponent,
    ChangepasswordComponent,
    NavigationComponent,
    ApproveModalComponent,
    RejectModalComponent,
    MoreInfoComponent,
    EntityInfoComponent,
    AppHistoryComponent,
    CmsModalComponent,
    PopupModalsComponent,
    AssignModalsComponent,
    AttachPopupComponent,
    ReprogramModalComponent,
    RequestModalComponent,
    PaidModalComponent,
    WatingModalComponent,
    RejectGrantModalComponent
  ],
  entryComponents: [
    ChangepasswordComponent,
    MoreInfoComponent,
    EntityInfoComponent,
    AppHistoryComponent,
    PopupModalsComponent,
    AssignModalsComponent,
    AttachPopupComponent
  ],

  providers: [AuthService
    , PermissionProvider
    , { provide: OWL_DATE_TIME_LOCALE, useValue: 'en-SG'}, SidenavService, ThemeService
  ]
})
export class PostAuthModule { }
