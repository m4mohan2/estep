import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
@Component({
  selector: 'app-budget',
  templateUrl: './budget.component.html',
  styleUrls: ['./budget.component.scss']
})
export class BudgetComponent implements OnInit {

  demoForm: FormGroup;
   
  arrayItems:any=[];

  arrayItems2:any=[];

  constructor(
    private _formBuilder: FormBuilder) 
    { 
      this.demoForm = this._formBuilder.group({
        demoArray: this._formBuilder.array([])
     });

  }

  ngOnInit() {
    this.arrayItems = [];
    this.arrayItems2 = [];
  }

get demoArray() {
    return this.demoForm.get('demoArray') as FormArray;
 }
 addItem(val, item) {   
   if(val == 'admin') {
    this.arrayItems.push(item);
    this.demoArray.push(this._formBuilder.control(false));
   }else if(val == 'direct') {
    this.arrayItems2.push(item);
    this.demoArray.push(this._formBuilder.control(false));
   }
 }
 removeItem(i) {
  this.arrayItems.pop();
  this.arrayItems2.pop();
    this.demoArray.removeAt(i);
 }

}
