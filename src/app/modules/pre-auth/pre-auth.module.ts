import { NgModule } from '@angular/core';
import { PreAuthComponent } from './pre-auth.component';
import { PreAuthRouteModule } from './pre-auth.routes.module';
import { SharedModule } from '../../shared/shared.module';
import { AuthService } from '../../services/apis/auth.service';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { ComponentsModule } from '../../components/components.module';

import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { VerifyComponent } from './verify/verify.component';
import { RegisteredComponent } from './registered/registered.component';
import { AccountVerifyComponent } from './account-verify/account-verify.component';

@NgModule({
  imports: [
    PreAuthRouteModule,
    ComponentsModule,
    SharedModule
  ],

  declarations: [PreAuthComponent, LoginComponent, RegisteredComponent, ForgotPasswordComponent, VerifyComponent, AccountVerifyComponent],
  exports: [],
  providers: [AuthService]
})
export class PreAuthModule { }
