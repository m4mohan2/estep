import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/apis/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { TranslateService } from '@ngx-translate/core';


@Component({
    selector: 'account-verify',
    templateUrl: './account-verify.component.html',
    styleUrls: ['./account-verify.component.scss']
  })
  export class AccountVerifyComponent implements OnInit {
    id: any;
    token: any;
    private sub: any;
    param: any;

    constructor(
        private translate: TranslateService,
        private route: ActivatedRoute,
        private authProvider: AuthService,
        private router: Router,
        private toast: ToastProvider,
        ) {}

    ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
       //this.id = +params['id']; // (+) converts string 'id' to a number
​       //console.log(this.id);
       //console.log(params);
       // In a real app: dispatch action to load the details here.

       this.param = {
        id: params['id'],
        token : params['token']
      };

       this.authProvider.accountdoverify(this.param).subscribe((responseData: any) => {
        
        if (responseData.success) {
          //if(responseData.user.user_type == 0){
            //this.toast.success(responseData.message);
            this.toast.success(this.translate.instant('MESSAGE.ACCOUNT_VERIFY_SUCCESS'));
            this.router.navigate(['pre-auth/login']);
          /*}else{
            this.router.navigate(['post-auth/dashboard']);
          }*/
        }
  
      }, error => {
        //this.loading = false;
      });

    });
  }

    
  }