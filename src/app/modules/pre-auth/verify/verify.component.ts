import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/apis/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { equalvalidator } from 'src/app/directives/validators/equal-validator';
import { NgxUiLoaderService,SPINNER } from 'ngx-ui-loader';


@Component({
    selector: 'email-verify',
    templateUrl: './verify.component.html',
    styleUrls: ['./verify.component.scss']
})
export class VerifyComponent implements OnInit {
    id: any;
    token: any;
    private sub: any;
    param: any;
    changePasswordform:FormGroup;
    varified:boolean =false;
    spinnerType = SPINNER.rectangleBounce;
    constructor(
        private route: ActivatedRoute,
        private authProvider: AuthService,
        private router: Router,
        private ngxService:NgxUiLoaderService,
        private toast: ToastProvider,
    ) { }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {

            this.param = {
                id: params['id'],
                token: params['token']
            };

            this.authProvider.doverify(this.param).subscribe((responseData: any) => {

                if (responseData.success) {
                    this.varified =true
                    this.toast.success(responseData.message);
                    
                }else{
                    this.varified =false
                }

            }, error => {
                console.log(error)
                this.varified =false
                this.toast.error(error.message);
            });

        });

        this.changePasswordform = new FormGroup({
            password: new FormControl('', [Validators.required]),
            confirmPassword: new FormControl('', [Validators.required, equalvalidator('password')])
        });
    }

    changePassword(passwordinfo){
        var password ={
            password:passwordinfo.password
        } 
        var entityId = this.param.id
        if (this.changePasswordform.valid) {
            this.ngxService.start()
            this.authProvider.resetPasswordForForgot(password,entityId).subscribe((responseData) => {
                if (responseData.success) {
                    this.ngxService.stop();
                    this.toast.success(responseData.message);
                    this.router.navigate(['/pre-auth/login']);
                }
            }, error => {
                this.ngxService.stop()
            });
        }
    }


}