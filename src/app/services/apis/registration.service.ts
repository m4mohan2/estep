import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AppConst } from '../../app.constants';


@Injectable()
export class RegistrationService  {
  private REGISTER: string;
  private VIEW: string;
  
  constructor(
    private httpClient: HttpClient
  ) {
    this.REGISTER = '/api/register';
    
   
}

  register(param): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.REGISTER
    };
    return this.httpClient.post<any>(request.url , param);
  }
  view(id): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.VIEW + '/' + id
    };
    return this.httpClient.get<any>(request.url);
  }

}
