import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AppConst } from '../../app.constants';

@Injectable()
export class MoreinfoService  {
    private ADD_MOREINFO: string;
    private LIST_MOREINFO: string;
    private MOREINFO_DELETE: string;
    constructor(
        private httpClient: HttpClient
    ) {
        this.ADD_MOREINFO = '/api/addMoreinfo';
        this.LIST_MOREINFO = '/api/moreinfoList';
        this.MOREINFO_DELETE = '/api/deleteMoreinfo';
    }

    addmoreinfo(moreinfo): Observable<any> {
        const request = {
            url: AppConst.API_BASE_URL + this.ADD_MOREINFO
        };
        return this.httpClient.post<any>(request.url, moreinfo);
    }

    listmoreinfo(proposal_id): Observable<any> {
        const request = {
          url: AppConst.API_BASE_URL + this.LIST_MOREINFO + '/' + proposal_id
        };
        return this.httpClient.get<any>(request.url);
    }

    moreinfodelete(id): Observable<any> {
    const request = {
        url: AppConst.API_BASE_URL + this.MOREINFO_DELETE + '/' + id
    };
    return this.httpClient.delete<any>(request.url );
    }

}