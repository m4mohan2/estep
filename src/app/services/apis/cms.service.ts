import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AppConst } from '../../app.constants';

@Injectable({
  providedIn: 'root'
})
export class CmsService {

  private ADD_CMS: string;
  private NEWS: string;
  private EDIT_CMS: string;
  private DELETE: string;
  private VIEW: string;
  constructor(
    private httpClient: HttpClient
  ) {
    this.ADD_CMS = '/api/addPage';
    this.NEWS = '/api/news';
    this.EDIT_CMS = '/api/editPage';
    this.DELETE = '/api/page';
    this.VIEW = '/api/page';
    
  }
  addCms(cmsinfo): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.ADD_CMS
    };
    return this.httpClient.post<any>(request.url, cmsinfo);
  }
  editCms(cmsinfo,id): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.EDIT_CMS + '/' + id
    };
    
    return this.httpClient.post<any>(request.url, cmsinfo);
  }
  view(id): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.VIEW + '/' + id
    };
    return this.httpClient.get<any>(request.url);
  }
  cmsList(param){
    const request = {
      url: AppConst.API_BASE_URL + this.VIEW 
    };
    return this.httpClient.get<any>(request.url,{params:param});
  }
  deleteCms(id){
    const request = {
      url: AppConst.API_BASE_URL + this.DELETE + '/' + id
    };
    return this.httpClient.delete<any>(request.url );
  }
}
