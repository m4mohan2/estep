import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AppConst } from '../../app.constants';


@Injectable()
export class VideoService  {
  private ADD_VIDEO: string;
  private VIDEO: string;
  private EDIT_VIDEO: string;
  private DELETE: string;
  private VIEW: string;
  private VIDEO_FILE_DEL :string;
  private ACTIVEVIDEO :string;
  private VIEWBYSLUG :string;
  constructor(
    private httpClient: HttpClient
  ) {
    this.ADD_VIDEO = '/api/addVideo';
    this.VIDEO = '/api/video';
    this.EDIT_VIDEO = '/api/editVideo';
    this.DELETE = '/api/video';
    this.VIEW = '/api/video';
    this.VIEWBYSLUG = '/api/viewBySlug';
    this.VIDEO_FILE_DEL = '/api/videoFileDelete';
    this.ACTIVEVIDEO = '/api/activeVideo';    
   
  }

  addvideo(videoinfo): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.ADD_VIDEO
    };
    return this.httpClient.post<any>(request.url, videoinfo);
  }

  video(param): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.VIDEO
    };
    return this.httpClient.get<any>(request.url,{params:param});
  }

  editvideo(videoinfo,id): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.EDIT_VIDEO + '/' + id
    };
   
    return this.httpClient.post<any>(request.url, videoinfo);
  }

  view(id): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.VIEW + '/' + id
    };
    return this.httpClient.get<any>(request.url);
  }
  viewBySlug(slug): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.VIEWBYSLUG + '/' + slug
    };
    return this.httpClient.get<any>(request.url);
  }

  

  delete(id): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.DELETE + '/' + id
    };
    return this.httpClient.delete<any>(request.url );
  }

  delFile(params): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.VIDEO_FILE_DEL +'/'+ params.id
    };
    return this.httpClient.delete<any>(request.url,{});  
  }

  activevideo(param): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.ACTIVEVIDEO
    };
    return this.httpClient.get<any>(request.url,{params:param});
  }
  

}
