import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConst } from '../../app.constants';


@Injectable()
export class EntitysearchService  {
  private USER_LIST: string;
  private ENTITY_SRCH: string;
  constructor(
    private httpClient: HttpClient
  ) {
    this.USER_LIST = '/api/userList';
    
    
   
}

  userlist(param): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.USER_LIST
    };
    
    return this.httpClient.get<any>(request.url , { params:param });
  }

  
  

}
