import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AppConst } from '../../app.constants';


@Injectable()
export class RefundService  {
 
  private QUALITY_REPORTS:string;
  
  private REFUND_LIST:string
  private REFUND_ADD:string
  private REFUND_EDIT:string
  private REFUND_VIEW:string
  private DELETE_REFUND:string

  constructor(
    private httpClient: HttpClient
  ) {
  
    this.QUALITY_REPORTS = '/api/getProposal';
   
    this.REFUND_LIST = '/api/refunds';
    this.REFUND_ADD = '/api/addRefund';
    this.REFUND_EDIT = '/api/editRefund';
    this.REFUND_VIEW = '/api/viewRefund';
    this.DELETE_REFUND = '/api/refund';
  }

  refundList(id): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.REFUND_LIST +'/'+id
    };
    return this.httpClient.get<any>(request.url);
  }

  addRefund(param){
    if(localStorage.getItem('refund_id')){
      const request = {
        url: AppConst.API_BASE_URL + this.REFUND_EDIT + '/' + localStorage.getItem('refund_id')
      };
      return this.httpClient.post<any>(request.url, param);
    }else{
      const request = {
        url: AppConst.API_BASE_URL + this.REFUND_ADD
      };
      return this.httpClient.post<any>(request.url, param);
    }
  }

  viewRefund(): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.REFUND_VIEW + '/' + localStorage.getItem('refund_id')
    };
    return this.httpClient.get<any>(request.url, {});  
  }

  deleteRefund(id){
    const request = {
      url: AppConst.API_BASE_URL + this.DELETE_REFUND +'/'+id
    };
    return this.httpClient.delete<any>(request.url);
  }

  qualityreports(id): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.QUALITY_REPORTS + '/' + localStorage.getItem('proposal_id')
    };
    return this.httpClient.get<any>(request.url);
  }


  
}
