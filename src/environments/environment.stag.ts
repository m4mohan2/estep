export const environment = {
	environmentName: 'Staging',
	production: true,
	api_base_url: 'http://34.196.238.149/cecfl/service/public',
	file_base_url: 'http://34.196.238.149/cecfl/service/public',
	assets_image_url: 'http://34.196.238.149/cecfl/assets/images',
	project_folder_url: 'http://34.196.238.149/cecfl',
};
