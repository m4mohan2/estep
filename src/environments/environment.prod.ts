export const environment = {
	environmentName: 'Production',
	production: true,
	api_base_url: 'https://www.impactocomunitariopr.org/estep-server/public',
	file_base_url: 'https://www.impactocomunitariopr.org/estep-server/public',
	assets_image_url: 'https://www.impactocomunitariopr.org/assets/images',
	project_folder_url: 'https://www.impactocomunitariopr.org',
};